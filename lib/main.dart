import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/provider/appData.dart';
import 'package:zmovil_conductor/provider/botDriver.dart';
import 'package:zmovil_conductor/provider/driver.dart';
import 'package:zmovil_conductor/provider/user.dart';
import 'package:zmovil_conductor/screen/botPage.dart';
import 'package:zmovil_conductor/screen/detalleCarreraPage.dart';
import 'package:zmovil_conductor/screen/earningScreen.dart';
import 'package:zmovil_conductor/screen/historyScreen.dart';
import 'package:zmovil_conductor/screen/homePage.dart';
import 'package:zmovil_conductor/screen/loginPage.dart';
import 'package:zmovil_conductor/screen/profileScreen.dart';
import 'package:zmovil_conductor/screen/ratingScreen.dart';
import 'package:zmovil_conductor/screen/ratingView.dart';
import 'package:zmovil_conductor/screen/splashScreen.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'helpers/style.dart'; // Importa esta línea

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await initializeDateFormatting('es'); // Inicializa localizaciones en español
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(create: (context) => UserProvider(),
        ),
        ChangeNotifierProvider<DriverProvider>(create: (context) => DriverProvider()),
        ChangeNotifierProvider<BotDriverProvider>(create: (context) => BotDriverProvider()),
        ChangeNotifierProvider<AppData>(
          create: (context) => AppData(
            Provider.of<UserProvider>(context, listen: false), // Obtener la instancia de UserProvider
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Conductor',
        home: SplashScreen(),
        //initialRoute: DetalleCarreraScreen.idScreen,
        routes: {
          SplashScreen.idScreen: (context) => const SplashScreen(), // Quita el punto y coma aquí
          LoginPage.idScreen: (context) => LoginPage(), // Quita el punto y coma aquí
          HistoryScreen.idScreen: (context) => HistoryScreen(), // Quita el punto y coma aquí
          ProfileScreen.idScreen: (context) => ProfileScreen(), // Quita el punto y coma aquí
          RatingView.idScreen: (context) => RatingView(), // Quita el punto y coma aquí
          BotPage.idScreen: (context) => BotPage(), // Quita el punto y coma aquí
          EarningsScreen.idScreen: (context) => EarningsScreen(), // Quita el punto y coma aquí
          HomePage.idScreen: (context) => const HomePage(), // Quita el punto y coma aquí
          RatingScreen.idScreen: (context) => RatingScreen(), // Quita el punto y coma aquí
        },
        theme: ThemeData(
          primarySwatch: Colors.yellow,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          appBarTheme: AppBarTheme(
            iconTheme: const IconThemeData(
              color: Colors.black,  // Cambia esto al color que prefieras
            ), toolbarTextStyle: const TextTheme(
            titleLarge: TextStyle(
              color: Colors.black,  // Cambia esto al color que prefieras
            ),
          ).bodyMedium, titleTextStyle: const TextTheme(
            titleLarge: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.w600// Cambia esto al color que prefieras
            ),
          ).titleLarge,
          ),
        ),
      ),
    );
  }
}
