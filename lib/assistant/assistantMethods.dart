import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/assistant/requestAssistant.dart';

import '../model/address.dart';
import '../model/directDetails.dart';
import '../model/driver.dart';
import '../model/history.dart';
import '../model/userModel.dart';
import '../provider/appData.dart';
import '../variables_constantes.dart';

class AssistantMehods {
  static Future<String> searchCoordinateAddress(Position position, context) async {
    String placeAddress = "";
    String st1, st2, st3, st4;
    String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=$mapKey";
    //String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=-17.861016,-63.211010&key=$mapKey";
    print(url);
    var response = await RequestAssistant.getRequest(url);
    if (response != "failed") {
      //placeAddress = response["results"][0]["formatted_address"];
      st1 = response["results"][1]["address_components"][0]["long_name"];
      st2 = response["results"][1]["address_components"][1]["short_name"];
//      st3 = response["results"][0]["address_components"][5]["long_name"];
//      st4 = response["results"][0]["address_components"][6]["long_name"];
      //placeAddress = st1 + ", " + st2 + ", " + st3 + ", " + st4;
      placeAddress = st1 + ", " + st2;
      Address userPickUpAddress = new Address();
      userPickUpAddress.longitude = position.longitude;
      //userPickUpAddress.longitude = -63.206994;
      userPickUpAddress.latitude = position.latitude;
      //userPickUpAddress.latitude = -17.827060;
      userPickUpAddress.placeName = placeAddress;

      Provider.of<AppData>(context, listen: false).updatePickUpLocationAddress(userPickUpAddress);
    }
    return placeAddress;
  }

  static Future<Position> _getLocation(context) async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
    return position;
  }


  static Future<DirectionDetails> obtainPlaceDirectionDetails(Position initialPosition, LatLng finalPosition) async {
    String directionUrl =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${initialPosition.latitude},${initialPosition.longitude}&destination=${finalPosition.latitude},${finalPosition.longitude}&key=$mapKey";
    //https://maps.googleapis.com/maps/api/directions/json?origin=-17.7842289,%20-63.1879632&destination=-17.7966436,%20-63.19136520000001&key=AIzaSyBVjYvx_la0Npt3Pnku4FKt_MZffCT6Dlo
    print(directionUrl);
    bool banderaRepetir = false;
    var res;

    res = await RequestAssistant.getRequest(directionUrl);
    if (res == "failed") {
      banderaRepetir = true;
    }

    if (res['status'] == 'OK') {
      banderaRepetir = true;
    }


    DirectionDetails directionDetails = DirectionDetails(
        distanceValue: res["routes"][0]["legs"][0]["distance"]["value"],
        durationValue: res["routes"][0]["legs"][0]["duration"]["value"],
        distanceText: res["routes"][0]["legs"][0]["distance"]["text"],
        durationText: res["routes"][0]["legs"][0]["duration"]["text"],
        encodePoints: res["routes"][0]["overview_polyline"]["points"]);
    directionDetails.encodePoints = res["routes"][0]["overview_polyline"]["points"];

    directionDetails.distanceText = res["routes"][0]["legs"][0]["distance"]["text"];
    directionDetails.distanceValue = res["routes"][0]["legs"][0]["distance"]["value"];

    directionDetails.durationText = res["routes"][0]["legs"][0]["duration"]["text"];
    directionDetails.durationValue = res["routes"][0]["legs"][0]["duration"]["value"];

    return directionDetails;
  }
  static void getCarrerasTomadas(context, String uid) async {
    String userId = uid;
    CollectionReference userCollection = FirebaseFirestore.instance.collection("drivers");
    DocumentSnapshot userSnapshot = await userCollection.doc(userId).get();
    Driver driverModel = Driver.fromSnapshot(userSnapshot);
    int rideCount=0;
// Verificar si el atributo rides existe y no es nulo
    if (driverModel.rides != null) {
      // Contar los registros con valor true en el atributo rides
      rideCount = driverModel.rides!.values.where((value) => value == true).length;

      // Imprimir el resultado
      print('Número de rides: $rideCount');
    } else {
      print('El atributo rides es nulo o no existe.');
    }



    //int rideCount = userModel?.rides?.length ?? 0;

    if (userSnapshot.exists) {
      Map<String, dynamic>? historyData = driverModel.rides;

      if (historyData != null) {
        // Actualizar el contador total de viajes
        int tripCounter = rideCount;
        Provider.of<AppData>(context, listen: false).updateTripsCounter(tripCounter);

        // Obtener las claves de los viajes
        List<String> tripHistoryKeys = historyData.keys.toList();
        Provider.of<AppData>(context, listen: false).updateTripKeys(tripHistoryKeys);

        // Obtener los datos de historial de viajes
        obtainTripRequestHistoryData(context);
      }
    }
  }

  static void obtainTripRequestHistoryData(context) {
    List<String> keys = Provider.of<AppData>(context, listen: false).tripHistoryKeys;
    CollectionReference<Map<String, dynamic>> historyCollection = FirebaseFirestore.instance.collection('historial');

    for (String key in keys) {
      historyCollection.doc(key).get().then((DocumentSnapshot<Map<String, dynamic>> snapshot) {
        if (snapshot.exists) {
          Map<String, dynamic> historyData = snapshot.data() as Map<String, dynamic>;
          Map<String, dynamic> rideInfoMap = historyData['rideInfoMap'];
          HistoryModel history = HistoryModel.fromMap(rideInfoMap); // Usar el constructor fromMap
          Provider.of<AppData>(context, listen: false).updateTripHistoryData(history);
        }
      });
    }
  }


}
