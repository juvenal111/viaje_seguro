import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:zmovil_conductor/helpers/constants.dart';

import '../model/driver.dart';

class DriverRepository {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //final Geoflutterfire _geo = Geoflutterfire();
  Stream<Driver?> get driver {
    return _auth.authStateChanges().switchMap((user) {
      if (user == null) {
        return Stream.value(null);
      } else {
        return _firestore.collection('drivers').doc(user.uid).snapshots().map((snapshot) {
          if (snapshot.exists) {
            return Driver.fromSnapshot(snapshot);
          } else {
            return null; // Return null when driver document doesn't exist
          }
        });
      }
    });
  }

  Future<bool> getEstado(String driverId) async {
    DocumentSnapshot docSnapshot = await _firestore.collection('drivers').doc(driverId).get();
    return docSnapshot.get('status');
  }

  Future<void> setDriverStatus(String driverId, bool status) async {
    try {
      await _firestore.collection('drivers').doc(driverId).update({
        'status': status,
      });
    } catch (e) {
      print(e);
    }
  }

  Future<User?> registerDriver(Driver driver, String password) async {
    try {
      // Obtener el último código utilizado en la colección "drivers"
      QuerySnapshot querySnapshot = await _firestore.collection('drivers').orderBy('code', descending: true).limit(1).get();
      int lastCode = querySnapshot.docs.isEmpty ? 0 : querySnapshot.docs.first['code'];

      // Crear un nuevo conductor con el código incrementado en 1
      Driver newDriver = Driver(
        name: driver.name,
        email: driver.email,
        phone: driver.phone,
        isBlocked: driver.isBlocked,
        code: lastCode + 1,
        status: driver.status,
        id: '',
      );

      // Registrar al nuevo conductor en la colección "drivers"
      UserCredential userCredential = await _auth.createUserWithEmailAndPassword(
        email: newDriver.email!,
        password: password,
      );

      await _firestore.collection('drivers').doc(userCredential.user!.uid).set({
        'id': userCredential.user!.uid,
        'name': newDriver.name!,
        'email': newDriver.email!,
        'phone': newDriver.phone!,
        'isBlocked': newDriver.isBlocked,
        'code': newDriver.code,
        'status': false,
      });

      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<bool> hasFilledCarInfo(String userId) async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance.collection('drivers').doc(userId).get();
    Map<String, dynamic>? data = documentSnapshot.data() as Map<String, dynamic>?;
    Map<String, dynamic>? carInfo = data?['info_car'] as Map<String, dynamic>?;
    if (carInfo != null &&
        carInfo['car_color'] != null &&
        carInfo['car_number'] != null &&
        carInfo['car_model'] != null &&
        carInfo['type'] != null &&
        ((carInfo['type'] == 'taxi' && carInfo['tipo_taxi'] != null) ||
            (carInfo['type'] == 'taxi-premiun') ||
            (carInfo['type'] == 'economico') ||
            (carInfo['type'] == 'bike' && carInfo['tipo_moto'] != null))) {
      return true;
    }
    return false;
  }

  // Método para escuchar los cambios en el estado de bloqueo del conductor
  Stream<bool> getDriverzBlockedStatus(String driverId) {
    return _firestore
        .collection('drivers')
        .doc(driverId)
        .snapshots()
        .map((docSnapshot) => docSnapshot.get('isBlocked'));
  }

  // Método para escuchar los cambios en el estado de bloqueo del conductor
  Stream<bool> getHasPaidBot(String driverId) {
    return _firestore
        .collection('drivers')
        .doc(driverId)
        .snapshots()
        .map((docSnapshot) {
      final data = docSnapshot.data();
      if (data != null && data.containsKey('has_paid_bot')) {
        return data['has_paid_bot'];
      } else {
        return false; // O cualquier otro valor predeterminado que desees
      }
    });
  }



}
