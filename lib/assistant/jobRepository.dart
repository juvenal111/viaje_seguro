import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:rxdart/rxdart.dart';
import '../model/job.dart';
import '../model/rideRequests.dart';

class JobRepository {
  final CollectionReference _jobsCollection = FirebaseFirestore.instance.collection('rideRequest');
  final BehaviorSubject<List<RideRequest>> _jobSubject = BehaviorSubject<List<RideRequest>>();

  Stream<List<RideRequest>> getJobs(GeoFirePoint currentPosition, double radius) {
    var geo = Geoflutterfire();

    Stream<List<DocumentSnapshot>> stream = geo.collection(collectionRef: _jobsCollection)
        .within(center: currentPosition, radius: radius, field: 'position', strictMode: true);

    stream.listen((List<DocumentSnapshot> querySnapshot) {
      List<RideRequest> rideRequests = [];
      for (var docSnapshot in querySnapshot) {
        rideRequests.add(RideRequest.fromMap(docSnapshot.data() as Map<String, dynamic>));
      }
      _jobSubject.add(rideRequests);
    });

    return _jobSubject.stream;
  }
  Stream<Map<String, dynamic>> streamStatusRideRequest(String rideId) {
    return FirebaseFirestore.instance
        .collection('rideRequests')
        .doc(rideId)
        .snapshots()
        .map((DocumentSnapshot snapshot) {
      final data = snapshot.data() as Map<String, dynamic>?; // Asegurar que los datos sean un Map<String, dynamic>
      final rideInfoMap = data?['rideInfoMap'];
      return rideInfoMap != null && rideInfoMap is Map<String, dynamic> ? rideInfoMap : {};
    });
  }


  Future<void> addJob(Job job) async {
    await _jobsCollection.add(job.toMap());
  }

  Future<void> updateJob(Job job) async {
    await _jobsCollection.doc(job.id).update(job.toMap());
  }

  Future<void> deleteJob(Job job) async {
    await _jobsCollection.doc(job.id).delete();
  }
}
