class Job {
  String id;
  String passengerName;
  String pickupAddress;
  String destinationAddress;
  double fare;
  String status;
  String title;

  Job({
    required this.id,
    required this.passengerName,
    required this.pickupAddress,
    required this.destinationAddress,
    required this.fare,
    required this.status,
    required this.title,
  });

  factory Job.fromMap(Map<String, dynamic> data, String documentId) {
    return Job(
      id: documentId,
      passengerName: data['passengerName'],
      pickupAddress: data['pickupAddress'],
      destinationAddress: data['destinationAddress'],
      fare: data['fare'].toDouble(),
      status: data['status'],
      title: data['title'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'passengerName': passengerName,
      'pickupAddress': pickupAddress,
      'destinationAddress': destinationAddress,
      'fare': fare,
      'status': status,
      'title': title,
    };
  }
}
