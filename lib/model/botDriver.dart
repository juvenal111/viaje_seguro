// class BotDriver {
//   double distanciaMaximaConductorOrigen;
//   double distanciaMaximaOrigenDestino;
//   String filtrarNotas;
//   String mensajeWhatsApp;
//   bool status;
//   double tarifaMaxima;
//   double tarifaMinima;
//   double tarifaPorKm;
//   int zonaTrabajo;
//   List<ZonaTrabajo> zonasTrabajo;
//   bool flagBot;
//
//   BotDriver({
//     required this.distanciaMaximaConductorOrigen,
//     required this.distanciaMaximaOrigenDestino,
//     required this.filtrarNotas,
//     required this.mensajeWhatsApp,
//     required this.status,
//     required this.tarifaMaxima,
//     required this.tarifaMinima,
//     required this.tarifaPorKm,
//     required this.zonaTrabajo,
//     required this.zonasTrabajo,
//     required this.flagBot,
//   });
//
//   factory BotDriver.fromMap(Map<String, dynamic> map) {
//     return BotDriver(
//       distanciaMaximaConductorOrigen: map['distancia_maxima_conductor_origen'],
//       distanciaMaximaOrigenDestino: map['distancia_maxima_origen_destino'],
//       filtrarNotas: map['filtrar_notas'],
//       mensajeWhatsApp: map['mensaje_whatsapp'],
//       status: map['status'],
//       tarifaMaxima: map['tarifa_maxima'],
//       tarifaMinima: map['tarifa_minima'],
//       tarifaPorKm: map['tarifa_por_km'],
//       zonaTrabajo: map['zona_trabajo'],
//       zonasTrabajo: List<ZonaTrabajo>.from(
//         map['zonas_trabajo'].map((zone) => ZonaTrabajo.fromMap(zone)),
//       ),
//       flagBot: map['flag_bot'],
//     );
//   }
//
//   Map<String, dynamic> toMap() {
//     return {
//       'distancia_maxima_conductor_origen': distanciaMaximaConductorOrigen,
//       'distancia_maxima_origen_destino': distanciaMaximaOrigenDestino,
//       'filtrar_notas': filtrarNotas,
//       'mensaje_whatsapp': mensajeWhatsApp,
//       'status': status,
//       'tarifa_maxima': tarifaMaxima,
//       'tarifa_minima': tarifaMinima,
//       'tarifa_por_km': tarifaPorKm,
//       'zona_trabajo': zonaTrabajo,
//       'zonas_trabajo': zonasTrabajo.map((zone) => zone.toMap()).toList(),
//       'flag_bot': flagBot,
//     };
//   }
//
//
// }
//
// class ZonaTrabajo {
//   Position center;
//   double radius;
//
//   ZonaTrabajo({
//     required this.center,
//     required this.radius,
//   });
//   factory ZonaTrabajo.fromMap(Map<String, dynamic> map) {
//     return ZonaTrabajo(
//       center: Position.fromMap(map['center']),
//       radius: map['radius'],
//     );
//   }
//
//   Map<String, dynamic> toMap() {
//     return {
//       'center': center.toMap(),
//       'radius': radius,
//     };
//   }
// }
//
// class Position {
//   double latitude;
//   double longitude;
//
//   Position({
//     required this.latitude,
//     required this.longitude,
//   });
//
//   factory Position.fromMap(Map<String, dynamic> map) {
//     return Position(
//       latitude: map['latitude'],
//       longitude: map['longitude'],
//     );
//   }
//
//   Map<String, dynamic> toMap() {
//     return {
//       'latitude': latitude,
//       'longitude': longitude,
//     };
//   }
// }
