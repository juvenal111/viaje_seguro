import 'package:cloud_firestore/cloud_firestore.dart';

class Driver {
  final String id;
  final String name;
  final String phone;
  final String email;
  final bool isBlocked;
  final int code;
  final String? carColor;
  final String? carModel;
  final String? carNumber;
  final String? tipoMoto;
  final String? tipoTaxi;
  final String? type;
  final double? earnings;
  final double? rating;
  final Map<String, bool>? rides;
  final bool status;

  Driver({
    required this.id,
    required this.name,
    required this.phone,
    required this.email,
    required this.isBlocked,
    required this.code,
    this.carColor,
    this.carModel,
    this.carNumber,
    this.tipoMoto,
    this.tipoTaxi,
    this.type,
    this.earnings,
    this.rating,
    this.rides,
    required this.status,
  });

  factory Driver.fromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
    return Driver(
      id: data['id'],
      name: data['name'],
      phone: data['phone'],
      email: data['email'],
      isBlocked: data['isBlocked'],
      code: data['code'],
      carColor: data['info_car'] != null ? data['info_car']['car_color'] : null,
      carModel: data['info_car'] != null ? data['info_car']['car_model'] : null,
      carNumber: data['info_car'] != null ? data['info_car']['car_number'] : null,
      tipoMoto: data['info_car'] != null ? data['info_car']['tipo_moto'] : null,
      tipoTaxi: data['info_car'] != null ? data['info_car']['tipo_taxi'] : null,
      type: data['info_car'] != null ? data['info_car']['type'] : null,
      earnings: data['earnings'] != null ? double.parse(data['earnings']) : null,
      rating: data['rating'] != null ? double.parse(data['rating']) : null,
      rides: data['rides'] != null ? Map<String, bool>.from(data['rides']) : null,
      status: data['status'],
    );
  }
  factory Driver.fromMap(Map<String, dynamic> data) {
    Map<String, dynamic> infoCar = data['info_car'] ?? {};

    return Driver(
      id: data['id'],
      name: data['name'],
      phone: data['phone'],
      email: data['email'],
      isBlocked: data['isBlocked'],
      code: data['code'],
      carColor: infoCar['car_color'],
      carModel: infoCar['car_model'],
      carNumber: infoCar['car_number'],
      tipoMoto: infoCar['tipo_moto'],
      tipoTaxi: infoCar['tipo_taxi'],
      type: infoCar['type'],
      earnings: data['earnings'] != null ? double.parse(data['earnings'].toString()) : null,
      rating: data['rating'] != null ? double.parse(data['rating'].toString()) : null,
      rides: data['rides'] != null ? Map<String, bool>.from(data['rides']) : null,
      status: data['status'],
    );
  }


  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'phone': phone,
      'email': email,
      'isBlocked': isBlocked,
      'code': code,
      'info_car': {
        'car_color': carColor,
        'car_model': carModel,
        'car_number': carNumber,
        'tipo_moto': tipoMoto,
        'tipo_taxi': tipoTaxi,
        'type': type,
      },
      'earnings': earnings?.toString(),
      'rating': rating?.toString(),
      'rides': rides,
      'status': status,
    };
  }

  @override
  String toString() {
    return 'Driver{name: $name, phone: $phone, email: $email, isBlocked: $isBlocked, code: $code, carColor: $carColor, carModel: $carModel, carNumber: $carNumber, tipoMoto: $tipoMoto, tipoTaxi: $tipoTaxi, type: $type, earnings: $earnings, rating: $rating, rides: $rides, status: $status}';
  }
}
