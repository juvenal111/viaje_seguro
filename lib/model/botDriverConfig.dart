import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zmovil_conductor/model/zonaTrabajo.dart';

class BotDriverConfig {
  BotDriverConfigA? configuracionA;
  BotDriverConfigB? configuracionB;
  BotDriverConfigC? configuracionC;

  BotDriverConfig({this.configuracionA, this.configuracionB, this.configuracionC});

  factory BotDriverConfig.fromMap(DocumentSnapshot<Map<String, dynamic>> snapshot) {
    Map<String, dynamic>? data = snapshot.data();

    return BotDriverConfig(
      configuracionA: BotDriverConfigA.fromSnapshot(data?["A"] ?? {}),
      configuracionB: BotDriverConfigB.fromSnapshot(data?["B"] ?? {}),
      configuracionC: BotDriverConfigC.fromSnapshot(data?["C"] ?? {}),
    );
  }
}


class BotDriverConfigA {
  final double tarifa_por_km;
  final double tarifa_minima;
  final double tarifa_maxima;
  final double distancia_maxima_conductor_origen;
  final double distancia_maxima_origen_destino;
  final String mensajeWhatsapp;
  final List<ZonaTrabajo> listaZonaTrabajo;
  final String filtrarNotas;

  BotDriverConfigA({
    required this.tarifa_por_km,
    required this.tarifa_minima,
    required this.tarifa_maxima,
    required this.distancia_maxima_conductor_origen,
    required this.distancia_maxima_origen_destino,
    required this.mensajeWhatsapp,
    required this.listaZonaTrabajo,
    required this.filtrarNotas,
  });

  factory BotDriverConfigA.fromSnapshot(Map<String, dynamic> data) {
    return BotDriverConfigA(
      tarifa_por_km: _toDouble(data["tarifa_por_km"]),
      tarifa_minima: _toDouble(data["tarifa_minima"]),
      tarifa_maxima: _toDouble(data["tarifa_maxima"]),
      distancia_maxima_conductor_origen: _toDouble(data["distancia_maxima_conductor_origen"]),
      distancia_maxima_origen_destino: _toDouble(data["distancia_maxima_origen_destino"]),
      mensajeWhatsapp: data["mensaje_whatsapp"] ?? "",
      listaZonaTrabajo: _devolverArray(data["zona_trabajo"]),
      filtrarNotas: data["filtrar_notas"] ?? "",
    );
  }

  static double _toDouble(dynamic value) {
    if (value is int) {
      return value.toDouble();
    } else if (value is double) {
      return value;
    } else {
      return 0.0; // Default value if it's not a valid number
    }
  }

  static List<ZonaTrabajo> _devolverArray(List<dynamic> snap) {
    List<ZonaTrabajo> listaZonaTrabajo = [];

    snap.forEach((value) {
      if (value is Map<String, dynamic>) {
        Map<String, dynamic> centerData = value["center"];
        Coordenadas coordenadas = Coordenadas(
          latitude: centerData["latitude"],
          longitude: centerData["longitude"],
        );
        double radius = (value["radius"] as double);

        ZonaTrabajo zonaTrab = ZonaTrabajo(
          center: coordenadas,
          radius: radius,
        );
        listaZonaTrabajo.add(zonaTrab);
      }
    });

    return listaZonaTrabajo;
  }


}

class BotDriverConfigB {
  final double distancia_maxima_conductor_origen;
  final String filtrarNotas;

  BotDriverConfigB({
    required this.distancia_maxima_conductor_origen,
    required this.filtrarNotas,
  });

  factory BotDriverConfigB.fromSnapshot(Map<String, dynamic> data) {
    //Map<String, dynamic>? data = snapshot.data();

    return BotDriverConfigB(
      distancia_maxima_conductor_origen: data?["distancia_maxima_conductor_origen"] ?? 0,
      filtrarNotas: data?["filtrar_notas"] ?? "",
    );
  }
}

class BotDriverConfigC {
  final String zonaDescanzo;

  BotDriverConfigC({
    required this.zonaDescanzo,
  });

  factory BotDriverConfigC.fromSnapshot(Map<String, dynamic> data) {
    //Map<String, dynamic>? data = snapshot.data();

    return BotDriverConfigC(
      zonaDescanzo: data?["zona_descanzo"] ?? "",
    );
  }
}
