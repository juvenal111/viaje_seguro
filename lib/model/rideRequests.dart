class RideRequest {
  String driverId;
  String riderId;
  String paymentMethod;
  Map<String, String> pickUpLocMap;
  Map<String, String> dropOffLocMap;
  String createdAt;
  String riderName;
  String riderPhone;
  String pickupAddress;
  String dropoffAddress;
  String distanceText;
  String durationText;
  String tipoVehiculo;
  String subtipoVehiculo;
  String carrerasTomadas;
  String tarifaSugerida;
  String tarifaOfertada;
  String deseo;
  String status;
  List<dynamic>? conductores = null;
  String tipoCarrera;
  Map<String, dynamic> conductoresDistanciaTiempo;
  double precioAceptado;
  String uid; // Agregado el campo uid

  RideRequest({
    required this.driverId,
    required this.riderId,
    required this.paymentMethod,
    required this.pickUpLocMap,
    required this.dropOffLocMap,
    required this.createdAt,
    required this.riderName,
    required this.riderPhone,
    required this.pickupAddress,
    required this.dropoffAddress,
    required this.distanceText,
    required this.durationText,
    required this.tipoVehiculo,
    required this.subtipoVehiculo,
    required this.carrerasTomadas,
    required this.tarifaSugerida,
    required this.tarifaOfertada,
    required this.deseo,
    required this.status,
    required this.conductores,
    required this.tipoCarrera,
    required this.conductoresDistanciaTiempo,
    required this.precioAceptado,
    required this.uid, // Agregado el campo uid
  });

  factory RideRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) {
      throw ArgumentError("Map cannot be null");
    }

    return RideRequest(
      driverId: map['driver_id'] ?? '',
      paymentMethod: map['payment_method'] ?? '',
      pickUpLocMap: Map<String, String>.from(map['pickup'] ?? {}),
      dropOffLocMap: Map<String, String>.from(map['dropOff'] ?? {}),
      createdAt: map['created_at'] ?? '',
      riderName: map['rider_name'] ?? '',
      riderPhone: map['rider_phone'] ?? '',
      pickupAddress: map['pickup_address'] ?? '',
      dropoffAddress: map['dropoff_address'] ?? '',
      distanceText: map['distanceText'] ?? '',
      durationText: map['durationText'] ?? '',
      tipoVehiculo: map['tipo_vehiculo'] ?? '',
      subtipoVehiculo: map['subtipo_vehiculo'] ?? '',
      carrerasTomadas: map['carreras_tomadas'] ?? '',
      tarifaSugerida: map['tarifa_sugerida'] ?? '',
      tarifaOfertada: map['tarifaOfertada'] ?? '',
      deseo: map['deseo'] ?? '',
      status: map['status'] ?? '',
      conductores: List<dynamic>.from(map['conductores'] ?? []),
      tipoCarrera: map['tipo_carrera'] ?? '',
      conductoresDistanciaTiempo: Map<String, dynamic>.from(map['conductores_distancia_tiempo'] ?? {}),
      precioAceptado: map['precio_aceptado'],
      uid: map['uid'] ?? '', // Agregado el campo uid
      riderId: map['rider_id'], // Agregado el campo uid
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'driver_id': driverId,
      'rider_id': riderId,
      'payment_method': paymentMethod,
      'pickup': pickUpLocMap,
      'dropOff': dropOffLocMap,
      'created_at': createdAt,
      'rider_name': riderName,
      'rider_phone': riderPhone,
      'pickup_address': pickupAddress,
      'dropoff_address': dropoffAddress,
      'distanceText': distanceText,
      'durationText': durationText,
      'tipo_vehiculo': tipoVehiculo,
      'subtipo_vehiculo': subtipoVehiculo,
      'carreras_tomadas': carrerasTomadas,
      'tarifa_sugerida': tarifaSugerida,
      'tarifaOfertada': tarifaOfertada,
      'deseo': deseo,
      'status': status,
      'conductores': List<dynamic>.from(conductores!),
      'tipo_carrera': tipoCarrera,
      'conductores_distancia_tiempo': conductoresDistanciaTiempo,
      'precio_aceptado': precioAceptado,
      'uid': uid, // Agregado el campo uid
    };
  }
  void setUid(String newUid) {
    uid = newUid;
  }
  bool isEquivalentTo(RideRequest other) {
    return driverId == other.driverId &&
        paymentMethod == other.paymentMethod &&
        tarifaSugerida == other.tarifaSugerida &&
        tarifaOfertada == other.tarifaOfertada &&
        conductoresDistanciaTiempo.toString() == other.conductoresDistanciaTiempo.toString();
  }
  void updateFrom(RideRequest other) {
    driverId = other.driverId;
    paymentMethod = other.paymentMethod;
    pickUpLocMap = Map<String, String>.from(other.pickUpLocMap);
    dropOffLocMap = Map<String, String>.from(other.dropOffLocMap);
    createdAt = other.createdAt;
    riderName = other.riderName;
    riderPhone = other.riderPhone;
    pickupAddress = other.pickupAddress;
    dropoffAddress = other.dropoffAddress;
    distanceText = other.distanceText;
    durationText = other.durationText;
    tipoVehiculo = other.tipoVehiculo;
    subtipoVehiculo = other.subtipoVehiculo;
    carrerasTomadas = other.carrerasTomadas;
    tarifaSugerida = other.tarifaSugerida;
    tarifaOfertada = other.tarifaOfertada;
    deseo = other.deseo;
    status = other.status;
    conductores = List<dynamic>.from(other.conductores!);
    tipoCarrera = other.tipoCarrera;
    conductoresDistanciaTiempo = Map<String, dynamic>.from(other.conductoresDistanciaTiempo);
    precioAceptado = other.precioAceptado;
    uid = other.uid;
  }
}
