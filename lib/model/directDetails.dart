import 'dart:core';

class DirectionDetails {
  int distanceValue;
  int durationValue;
  String distanceText;
  String durationText;
  String encodePoints;

  DirectionDetails(
      {required this.distanceValue, required this.durationValue, required this.distanceText, required this.durationText, required this.encodePoints});
}