import 'package:cloud_firestore/cloud_firestore.dart';

class ZonaTrabajo {
  Coordenadas center;
  double radius;

  ZonaTrabajo({
    required this.center,
    required this.radius,
  });
}

class Coordenadas {
  double latitude;
  double longitude;

  Coordenadas({
    required this.latitude,
    required this.longitude,
  });
}
