import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  static const ID = "id";
  static const NAME = "name";
  static const EMAIL = "email";
  static const PHONE = "phone";
  static const VOTES = "votes";
  static const TRIPS = "trips";
  static const RATING = "rating";
  static const TOKEN = "token";

  String? _id;
  String? _name;
  String? _email;
  String? _phone;
  String? _token;
  int? _votes;
  int? _trips;
  double? _rating;

  String? get name => _name;
  String? get email => _email;
  String? get id => _id;
  String? get token => _token;
  String? get phone => _phone;
  int? get votes => _votes;
  int? get trips => _trips;
  double? get rating => _rating;

  UserModel.fromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
    _name = data[NAME] as String? ?? "N/A";
    _email = data[EMAIL] as String? ?? "N/A";
    _id = data[ID] as String? ?? "N/A";
    _token = data[TOKEN] as String? ?? "N/A";
    _phone = data[PHONE] as String? ?? "N/A";
    _votes = data[VOTES] as int? ?? 0;
    _trips = data[TRIPS] as int? ?? 0;
    _rating = data[RATING] as double? ?? 0.0;
  }
}
