import 'package:cloud_firestore/cloud_firestore.dart';

class UpdateChecker {
  static Future<bool> checkForUpdates() async {
    try {
      final CollectionReference appVersionCollection =
      FirebaseFirestore.instance.collection('app_versions');

      DocumentSnapshot snapshot =
      await appVersionCollection.doc('latest_version').get();

      if (snapshot.exists) {
        final Map<String, dynamic>? data = snapshot.data() as Map<String, dynamic>?;
        final String latestVersion = data?['version_conductor'] ?? '';

        final String currentVersion = '1.0.0'; // Obtén la versión actual de tu app
        return latestVersion != currentVersion;
      } else {
        print('El documento latest_version no existe en Firestore');
        return false; // No hay actualizaciones disponibles
      }
    } catch (e) {
      print('Error al verificar actualizaciones: $e');
      return false; // En caso de error, no bloquear la app
    }
  }
}
