import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:zmovil_conductor/assistant/driverRepository.dart';
import 'package:zmovil_conductor/screen/carInfoScreen.dart';
import 'package:zmovil_conductor/screen/splashScreen.dart';

import '../assistant/userRepository.dart';
import '../model/driver.dart';
import '../screen/homePage.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  late DriverRepository driverRepository;
  late final FirebaseAuth _auth;
  late final FirebaseFirestore _db;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    userRepository = UserRepository();
    driverRepository = DriverRepository();
    _auth = FirebaseAuth.instance;
    _db = FirebaseFirestore.instance;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registro'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(
                labelText: 'Nombre completo',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingrese su nombre completo';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _emailController,
              decoration: InputDecoration(
                labelText: 'Correo electrónico',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingrese su correo electrónico';
                }
                if (!value.contains('@')) {
                  return 'Por favor, ingrese un correo electrónico válido';
                }
                return null;              },
            ),
            TextFormField(
              controller: _phoneController,
              decoration: InputDecoration(
                labelText: 'Teléfono',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingrese su número de teléfono';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _passwordController,
              obscureText: true,
              decoration: InputDecoration(
                labelText: 'Contraseña',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingrese su contraseña';
                }
                if (value.length < 6) {
                  return 'La contraseña debe tener al menos 6 caracteres';
                }
                return null;
              },
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                if (validateInputs()) {
                  registerUser();
                }
              },
              child: Text('Registrarse'),
            ),
          ],
        ),
      ),
    );
  }

  bool validateInputs() {
    return _nameController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _phoneController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        _emailController.text.contains('@') &&
        _passwordController.text.length >= 6;
  }

  void registerUser() async {
    final driver = Driver(
      id: '',
      name: _nameController.text,
      email: _emailController.text,
      phone: _phoneController.text,
      isBlocked: false,
      code: 0,
      status: false,
    );
    try {
      // Registramos al usuario en el repositorio
      User? user = await driverRepository.registerDriver(driver, _passwordController.text);
      // Una vez registrado el usuario, lo logueamos automáticamente
      _login();
    } catch (e) {
      // Si hubo un error durante el registro, lo mostramos en pantalla
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Error'),
            content: Text('No se pudo registrar al usuario: ${e.toString()}'),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }
  }

  Future<void> _login() async {
    //await driverRepository.login(_emailController.text, _passwordController.text);
    // Redirigimos al usuario a la pantalla de inicio


    try {
      final UserCredential userCredential = await _auth.signInWithEmailAndPassword(
        email: _emailController.text.trim(),
        password: _passwordController.text.trim(),
      );

      final User user = userCredential.user!;
      final DocumentSnapshot userDoc = await _db.collection('drivers').doc(user.uid).get();

      if (userDoc.exists) {
        //await _requestLocationPermission();
        if (mounted) {

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (_) => SplashScreen()),
          );
        }
      } else {
        await _auth.signOut();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Error: Este usuario no tiene una cuenta'),
            backgroundColor: Colors.red,
          ),
        );
      }
    } on FirebaseAuthException catch (e) {
      // Mostrar mensaje de error
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Error: ${e.message}'),
          backgroundColor: Colors.red,
        ),
      );
    }
  }
}
