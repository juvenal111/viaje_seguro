import 'package:flutter/cupertino.dart';
import 'package:zmovil_conductor/provider/user.dart';

import '../model/address.dart';
import '../model/history.dart';



class AppData extends ChangeNotifier{
  late Address pickUpLocation, dropOffLocation;
  int _countTrips = 0;
  List<String> tripHistoryKeys = [];
  List<HistoryModel> tripHistoryDataList = [];
  UserProvider userProvider;

  AppData(this.userProvider);

  int get countTrips => _countTrips;

  void updateCountTrips(int newCount) {
    _countTrips = newCount;
    notifyListeners();
  }
  void updateTripKey(String newKey){
    tripHistoryKeys.add(newKey);
    notifyListeners();
  }
  void updatePickUpLocationAddress(Address pickUpAddress){
    pickUpLocation = pickUpAddress;
    notifyListeners();
  }
  void updateDropOffLocationAddress(Address dropOffAddress){
    print('updateDropOffLocationAddress');
    dropOffLocation = dropOffAddress;
    notifyListeners();
  }

  void updateTripsCounter(int tripCounter){
    _countTrips = tripCounter;
    notifyListeners();
  }
  void updateTripKeys(List<String> newKeys){
    tripHistoryKeys = newKeys;
    notifyListeners();
  }
  void updateTripHistoryData(HistoryModel eachHistory){
    tripHistoryDataList.add(eachHistory);
    notifyListeners();
  }




}


