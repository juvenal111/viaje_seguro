import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zmovil_conductor/model/botDriver.dart';
import 'package:zmovil_conductor/model/botDriverConfig.dart';
import '../helpers/constants.dart';
import '../model/driver.dart';
import '../services/botDriver.dart';
import '../services/user.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated, InfoCar }

class UserProvider with ChangeNotifier {
  static const LOGGED_IN = "loggedIn";
  static const ID = "id";
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final BotDriverService _botDriverService = BotDriverService();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late int _countLogin = 0;

  User? _user;
  Status _status = Status.Uninitialized;
  UserServices _userServices = UserServices();
  Driver? _userModel;
  BotDriverConfig? _botDriverData;
//  getter
  int get countLogin => _countLogin;
  Status get status => _status;
  bool isRegister = false;
  User? get user => _user;

  Driver? get userModel => _userModel;


  BotDriverConfig? get botDriverData => _botDriverData;

  late final FirebaseAuth _firebaseAuth;

  UserProvider() {
    _auth.authStateChanges().listen(_onAuthStateChanged);
  }


  // UserProvider.initialize() {
  //   _initialize();
  //   // _auth.authStateChanges().listen(_onAuthStateChanged);
  // }
  void _onAuthStateChanged(User? user) {
    if (user == null) {
      _status = Status.Unauthenticated;
    } else {
      _user = user;
      _status = Status.Authenticated;
      fetchUserData();
      fetchBotDriverData();
      print("authenticated:");
    }
    notifyListeners();
  }
  // public variables
  final formkey = GlobalKey<FormState>();

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();

  Future<void> fetchBotDriverData() async {
    try {
      if (_user != null) {
        String userId = _user!.uid;
        _botDriverData = (await _botDriverService.getBotDriverConfig(userId));
        notifyListeners();
      }
    } catch (e) {
      print('Error obteniendo datos de botDriver: $e');
      _botDriverData = null;
    }
  }

  Future<bool> signIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    DocumentSnapshot? passengerSnapshot;
    try {
      _status = Status.Authenticating;
      notifyListeners();


     // await _auth.signInWithEmailAndPassword(email: email.text.trim(), password: password.text.trim());




      await auth.signInWithEmailAndPassword(email: email.text.trim(), password: password.text.trim()).then((value) async {


        // Obtener el userModel del servicio
        //_user = value.user;
        _userModel = await _userServices.getUserById(value.user!.uid);

        await prefs.setString(ID, value.user!.uid);
        await prefs.setBool(LOGGED_IN, true);
        passengerSnapshot = await FirebaseFirestore.instance.collection('drivers').doc(value.user!.uid).get();
        await fetchBotDriverData();
        _countLogin++;
        //_status = Status.Authenticated;
        notifyListeners();
      });

      if (!passengerSnapshot!.exists) {
        // El usuario no es un pasajero, lanzar excepción
        await auth.signOut();
        throw Exception("Este usuario no tiene permisos para acceder como pasajero");
      } else {

      }
      //await _initialize();
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      //throw e;
      return false;
    }
  }


  Future<bool> signUp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      _status = Status.Authenticating;
      notifyListeners();

      // Registra al usuario en Firebase Auth
      await auth.createUserWithEmailAndPassword(email: email.text.trim(), password: password.text.trim());
      isRegister = true;
      // Obtiene la última categoría disponible
      QuerySnapshot querySnapshot = await _firestore.collection('drivers').orderBy('code', descending: true).limit(1).get();
      int lastCode = querySnapshot.docs.isEmpty ? 0 : querySnapshot.docs.first['code'];

      // Crea el usuario en Firestore
      await _userServices.createUser(
        id: auth.currentUser!.uid,
        name: name.text.trim(),
        email: email.text.trim(),
        phone: phone.text.trim(),
        code: lastCode + 1,
      );

      // Actualiza el estado de autenticación
      //_status = Status.Authenticated;
      //_userModel = await _userServices.getUserById(auth.currentUser!.uid);
      await prefs.setString(ID, auth.currentUser!.uid);
      await prefs.setBool(LOGGED_IN, true);
      _countLogin++;
     //_status = Status.InfoCar;
      //fetchUserData();
      notifyListeners();
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      print(e.toString());
      return false;
    }
  }



  Future signOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();


    _status = Status.Unauthenticated;
    await prefs.setString(ID, "");
    await prefs.setBool(LOGGED_IN, false);
    auth.signOut();
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  void clearController() {
    name.text = "";
    password.text = "";
    email.text = "";
    phone.text = "";
  }




  void fetchUserData() {
    String? userId = _user?.uid;
    print('user_id');
    print(userId);
    if (userId != null) {
      FirebaseFirestore.instance.collection('drivers').doc(userId).snapshots().listen((snapshot) {
        print('fetchUserData');
        //escuchando
        if (snapshot.exists) {
          // Obtener los datos del documento
          Map<String, dynamic> userData = snapshot.data() as Map<String, dynamic>;

          // Actualizar el userModel con los nuevos datos
          // if(!isRegister){
          //   _userModel = Driver.fromMap(userData);
          // }
          _userModel = Driver.fromMap(userData);
          print(_userModel.toString());

          // Notificar a los listeners que se ha actualizado el userModel
          notifyListeners();
        }
      });
    }
  }


}

