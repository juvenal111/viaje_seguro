import 'package:flutter/material.dart';
import 'package:zmovil_conductor/model/botDriverConfig.dart';

import '../services/botDriver.dart';

class BotDriverProvider with ChangeNotifier {
  final BotDriverService _botDriverService = BotDriverService();
  BotDriverConfig? _botDriverConfig;

  BotDriverConfig? get botDriverConfig => _botDriverConfig;

  bool get hasBotDriverConfig => _botDriverConfig != null;


  // Agregamos una nueva variable para verificar si los parámetros están configurados
  bool get parametrosConfigurados {
    if (_botDriverConfig != null) {
      BotDriverConfigA? configA = _botDriverConfig!.configuracionA;
      BotDriverConfigB? configB = _botDriverConfig!.configuracionB;
      BotDriverConfigC? configC = _botDriverConfig!.configuracionC;

      return configA != null &&
          configA.distancia_maxima_conductor_origen != 0 ||
          configA?.distancia_maxima_origen_destino != 0 ||
          configA?.tarifa_maxima != 0 ||
          configA?.tarifa_minima != 0 ||
          configA?.tarifa_por_km != 0 ||
          configA?.mensajeWhatsapp != '' || // Verificamos si mensajeWhatsapp no está vacío
          configA?.filtrarNotas != '' || // Verificamos si filtrarNotas no está vacío
          configA!.listaZonaTrabajo.isNotEmpty;
    } else {
      return false;
    }
  }



  Future<void> fetchBotDriverConfig(String userId) async {
    try {
      _botDriverConfig = await _botDriverService.getBotDriverConfig(userId);
      notifyListeners();
    } catch (e) {
      print('Error obteniendo configuración de botDriver en el provider: $e');
      _botDriverConfig = null;
    }
  }
}
