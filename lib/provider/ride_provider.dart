// import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
//
// class RideProvider extends ChangeNotifier {
//   final RideService _rideService = RideService();
//   bool _isBotBusy = false;
//
//   bool get isBotBusy => _isBotBusy;
//
//   // Método para iniciar el bot y manejar las carreras
//   Future<void> startBot(Position position) async {
//     if (!_isBotBusy) {
//       _isBotBusy = true;
//
//       await _rideService.startBot(position);
//
//       _isBotBusy = false;
//     }
//   }
//
// // Más métodos relacionados con las carreras y el bot...
//
// }
