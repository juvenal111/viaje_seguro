import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DriverProvider extends ChangeNotifier {
  Future<bool> hasFilledCarInfo(String userId) async {
    DocumentSnapshot documentSnapshot =
    await FirebaseFirestore.instance.collection('drivers').doc(userId).get();
    Map<String, dynamic>? data = documentSnapshot.data() as Map<String, dynamic>?;
    Map<String, dynamic>? carInfo = data?['info_car'] as Map<String, dynamic>?;
    if (carInfo != null &&
        carInfo['car_color'] != null &&
        carInfo['car_number'] != null &&
        carInfo['car_model'] != null &&
        carInfo['type'] != null &&
        ((carInfo['type'] == 'taxi') || (carInfo['type'] == 'xl_+_4_personas') || (carInfo['type'] == 'moto') ||
            carInfo['type'] == 'torito')) {
      return true;
    }
    return false;
  }
  //final List<String> _cities = ['elija', 'taxi', 'XL + 4 pasajeros', 'moto', 'torito'];
  // Agrega aquí tus propiedades y métodos.
}