import 'dart:async';
import 'dart:io';
import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:synchronized/synchronized.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zmovil_conductor/model/rideRequests.dart';
import 'package:zmovil_conductor/screen/ratingScreen.dart';
import 'package:zmovil_conductor/variables_constantes.dart';
import '../assistant/assistantMethods.dart';
import '../assistant/jobRepository.dart';
import '../assistant/mapKitAssistant.dart';
import '../helpers/constants.dart';
import '../helpers/style.dart';
import '../model/botDriverConfig.dart';
import '../model/directDetails.dart';
import '../model/driver.dart';
import '../model/history.dart';
import '../provider/appData.dart';
import '../services/botDriver.dart';
import '../services/ride_requests.dart';
import '../widget/CollectFareDialog.dart';
import '../widget/progressDialog.dart';
import 'dart:math' as Math;

class DetalleCarreraScreen extends StatefulWidget {
  static const String idScreen = "detalleCarrera";
  final RideRequest rideDetails;
  final String hash;
  final Driver driver;
  final DocumentSnapshot<Map<String, dynamic>>? history;
  final bool? makeOffer;
  final BotDriverConfig? config;
  final VoidCallback? stopRidesSubscription;
  final Position currrentPosition;

  DetalleCarreraScreen(
      {Key? key, this.config, required this.currrentPosition, this.stopRidesSubscription, required this.rideDetails, required this.hash, required this.driver, this.history, this.makeOffer})
      : super(key: key);

  @override
  _DetalleCarreraScreenState createState() => _DetalleCarreraScreenState();
}

class _DetalleCarreraScreenState extends State<DetalleCarreraScreen> {
  final CollectionReference _rideRequestsCollection = FirebaseFirestore.instance.collection('rideRequests');
  final CollectionReference _historialCollection = FirebaseFirestore.instance.collection('historial');
  final CollectionReference _driverCollection = FirebaseFirestore.instance.collection('drivers');
  late Geoflutterfire geo = Geoflutterfire();

  // Declarar el objeto de bloqueo
  final _lock = Lock();

  late DocumentSnapshot<Map<String, dynamic>> history;

  ///listener
  StreamSubscription<DocumentSnapshot>? _rideRequestSubscription; // Variable para el oyente
  late StreamSubscription<Position> _positionStreamSubscription;
  final GeolocatorPlatform _geolocator = GeolocatorPlatform.instance;
  StreamSubscription<Map<String, dynamic>>? estadoSubscription;

  final JobRepository _jobRepository = JobRepository(); // Crea una instancia de la clase JobRepository

  late Timer timer;
  Timer? _timerOffer;

  int durationCounter = 0;
  LatLng? _previousLocation;
  Color btnColor = Colors.blueAccent;
  late GoogleMapController mapController;
  double tarifaOfertada = 0.0;
  double tarifaSugerida = 0.0;
  List<Marker> _markers = [];
  late double speed = 0.0;
  final radius = 15.0; // en km
  late Position currentPosition;
  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42, -122.0058),
    zoom: 14.4746,
  );
  Set<Polyline> polYlineSet = {};
  bool isVisiblePositioned1 = false;
  bool isVisiblePositioned2 = false;
  String distanceTextOrigenDestino = '';
  String durationTextOrigenDestino = '';
  String distanceTextConductorPasajero = '';
  String durationTextConductorPasajero = '';
  BitmapDescriptor? customIcon;
  String btnTitle = 'En puerta';
  late LatLng p2;
  late RideRequest rideDetails;
  late String hash;
  bool verMapa = false;
  String statusRide = '';
  late Driver driver;

  String distanciaConductorPasajero = '';
  String tiempoConductorPasajero = '';

  bool isPositionStreamSubscription = false;
  bool _isButtonDisabledCancelar = false; // Variable para controlar el estado del botón
  bool _isButtonDisabled = false;
  bool isBack = true;

  final rideRequestServices = RideRequestServices();

  @override
  void initState() {
    if (widget.stopRidesSubscription != null) {
      print('stop position');
      widget.stopRidesSubscription!();
    }
    super.initState();
    //startStreamStatus(widget.hash);
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(80, 80)), 'images/car_android.png').then((icon) {
      setState(() {
        customIcon = icon;
      });
    });
    if (widget.history != null) {
      history = widget.history!;
    }
    tarifaOfertada = double.tryParse(widget.rideDetails.tarifaSugerida) ?? 0.0;
    print('tarifa ofertada: ' + tarifaOfertada.toString());
    rideDetails = widget.rideDetails;
    hash = widget.hash;
    driver = widget.driver;
    init();
    setState(() {
      p2 = LatLng(double.parse(widget.rideDetails.pickUpLocMap['latitude'].toString()), double.parse(widget.rideDetails.pickUpLocMap['longitude'].toString()));
    });

    if (widget.rideDetails.tipoCarrera != 'taxi_a_la_ubicacion') {
      // Iniciar la escucha de la propiedad tarifaOfertada
      rideRequestServices.startListeningToTarifaSugerida(hash, (newTarifaSugerida) {
        // Manejar el cambio de tarifaOfertada aquí
        print('Nueva tarifaOfertada: $newTarifaSugerida');
        setState(() {
          tarifaSugerida = newTarifaSugerida;
        });
      });
    }

    if (widget.makeOffer != null) {
      setState(() {
        tarifaSugerida = double.parse(widget.rideDetails.tarifaSugerida);
      });

      if (widget.makeOffer!) {
        Future.delayed(Duration(seconds: 5), () {
          makeOffer();
        });
      } else {
        Future.delayed(Duration(seconds: 3), () async {
          await acceptRideRequest(hash);
        });
      }
    } else {
      print('makeOffer es nulo');
    }
  }

  Future<void> makeOffer() async {
    BotDriverService _botDriverService = BotDriverService();
    // Obtener la configuración del bot para el usuario actual
    BotDriverConfig? botDriverConfig = await _botDriverService.getBotDriverConfig(currentfirebaseUser.uid);
    BotDriverConfigA? configuracionA = botDriverConfig?.configuracionA;

    ///distancia origen-destino
    double? distanciaMaximaOrigenDestino = configuracionA?.distancia_maxima_origen_destino;
    String distanceString = rideDetails.distanceText;
    String numericString = distanceString.replaceAll(RegExp(r'[^0-9.]'), ''); // Elimina caracteres no numéricos
    double distanciaOrigenDestino = double.tryParse(numericString) ?? 0.0; // Convierte a double, si no se puede, asigna 0.0
    print(distanciaMaximaOrigenDestino);
    print(distanciaOrigenDestino);

    /***START OFERTAR TARIFA**/
    double? tarifa_por_km_bot = configuracionA?.tarifa_por_km;
    double precioTotal_actual = double.parse(rideDetails.tarifaOfertada); // Precio total actual conocido

    // Calcular el nuevo precio total necesario para ajustarse al precio por kilómetro constante
    double nuevoPrecioTotal = tarifa_por_km_bot! * distanciaOrigenDestino;

    // Calcular la diferencia entre el nuevo precio total y el precio total actual
    double diferencia = nuevoPrecioTotal - precioTotal_actual;

    setState(() {
      _isButtonDisabled = true;
      tarifaOfertada = tarifaOfertada + diferencia;
    });
    ofrecerTarifa();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _stopPositionStream() {
    print("stop stream");
    if (isPositionStreamSubscription) {
      _positionStreamSubscription.cancel();
    }
  }

  Future<void> init() async {
    if (widget.history != null) {
      statusRide = history['status'];
      print(statusRide);
      if (statusRide == "aceptada" || statusRide == "arrived" || statusRide == "onride") {
        setState(() {
          isBack = false;
        });
        //iniciar envio de ubicacion en tloiempo real

        if (statusRide == "arrived") {
          setState(() {
            btnTitle = "Iniciar viaje";
            verMapa = true;
            btnColor = secundary;
          });
        }
        if (statusRide == "onride") {
          setState(() {
            btnTitle = "Fin de viaje";
            btnColor = primary;
          });
        }
        if (statusRide == "finished") {
          await endTheTrip();
        }

        //oculto positioned
        setState(() {
          isVisiblePositioned1 = false;
          isVisiblePositioned2 = true;
        });
      }
    } else {
      //startStreamStatus(widget.hash);
      setState(() {
        isVisiblePositioned1 = true;
        isVisiblePositioned2 = false;
      });
    }
  }

  Future<void> locatePosition() async {
    LatLng latLatPosition = LatLng(widget.currrentPosition.latitude, widget.currrentPosition.longitude);
    CameraPosition cameraPosition = CameraPosition(target: latLatPosition, zoom: 14);
    mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  @override
  Widget build(BuildContext context) {
    //createIconMarker();
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Detalle carrera'),
          backgroundColor: primary,
        ),
        body: Stack(
          children: [
            GoogleMap(
              myLocationButtonEnabled: true,
              myLocationEnabled: false,
              compassEnabled: true,
              rotateGesturesEnabled: true,
              polylines: polYlineSet,
              initialCameraPosition: _kGooglePlex,
              markers: Set<Marker>.of(_markers),
              onMapCreated: (GoogleMapController controller) async {
                mapController = controller;
                if (statusRide == "aceptada" || statusRide == "arrived" || statusRide == "onride") {
                  _initPositionStream('historial');
                }
                await locatePosition();
                calculateRoute();
              },
              // onTap: (position) {
              //   dropOffPosition = position;
              // },
            ),

            ///boton buscador
            Visibility(
              visible: isVisiblePositioned2,
              child: Positioned(
                bottom: 177.0,
                right: 10.0,
                child: Container(
                  color: Colors.transparent,
                  child: Column(
                    children: [
                      InkWell(
                        onTap: () {
                          openwhatsapp(widget.config!.configuracionA!.mensajeWhatsapp);
                        },
                        child: Image.asset("images/whatsapp.png", height: 80.0),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: isVisiblePositioned2,
              child: Positioned(
                bottom: 180.0,
                left: 10.0,
                child: Container(
                  width: 200,
                  height: 50,
                  child: ElevatedButton.icon(
                    onPressed: () {
                      _openMapTaxiALaUbicacionRecogerPasajero();

                      // if (rideDetails.tipoCarrera == 'taxi_a_la_ubicacion') {
                      // } else {
                      //   print('_openMap');
                      //   _openMap();
                      // }
                    },
                    icon: Transform.rotate(
                      angle: 20 * 3.1416 / 180, // Ángulo de rotación en radianes (20 grados)
                      child: Icon(Icons.navigation),
                    ),
                    label: Text(
                      "Navegar",
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      backgroundColor: Colors.grey,
                      padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                    ),
                  ),
                ),
              ),
            ),
            isVisiblePositioned1
                ? Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 0.0,
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black38,
                            blurRadius: 16.0,
                            spreadRadius: 0.5,
                            offset: Offset(0.7, 0.7),
                          ),
                        ],
                      ),
                      height: 250,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                        child: Column(
                          children: [
                            // ///categoria
                            // Expanded(
                            //   child: Row(
                            //     children: [
                            //       Expanded(
                            //         flex: 3,
                            //         child: Column(
                            //           mainAxisSize: MainAxisSize.min,
                            //           crossAxisAlignment: CrossAxisAlignment.start,
                            //           children: <Widget>[
                            //             const Text(
                            //               "Categoria:",
                            //               style: TextStyle(fontSize: 10.0, color: Colors.black38),
                            //             ),
                            //             Expanded(
                            //               child: Row(
                            //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //                 children: [
                            //                   Expanded(
                            //                     child: Row(
                            //                       children: [
                            //                         Expanded(
                            //                           child: Text(
                            //                             rideDetails.tipoVehiculo,
                            //                             style: Theme.of(context).textTheme.titleMedium,
                            //                           ),
                            //                         ),
                            //                       ],
                            //                     ),
                            //                   ),
                            //                   //SizedBox(width: 2),
                            //                 ],
                            //               ),
                            //             ),
                            //           ],
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                child: Row(
                                  children: [
                                    //column profile
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          crossAxisAlignment: CrossAxisAlignment.center,

//                        mainAxisSize: MainAxisSize.min,
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.all(Radius.circular(13)),
                                              child: Container(
                                                height: 55,
                                                width: 55,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(15),
                                                  color: secundary,
                                                ),
                                                child: Image.asset(
                                                  "images/pasajero.png",
                                                  //height: 50,
                                                  //width: 50,
                                                  fit: BoxFit.contain,
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                                child: Column(
                                              children: [
                                                Text(
                                                  '${rideDetails.riderName}',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontSize: 10.0,
                                                    color: Colors.black38,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    'Carr.: ${rideDetails.carrerasTomadas}',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 10.0,
                                                      color: Colors.black38,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )),
                                          ],
                                        ),
                                      ),
                                    ),
                                    //columns tarifa y otros datos
                                    Expanded(
                                      flex: 8,
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 00.0, 0.0),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            ///pasajero destino
                                            Expanded(
                                              flex: 4,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      padding: EdgeInsets.fromLTRB(10, 1, 1, 1),
                                                      color: Colors.blueAccent,
                                                      child: Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[

                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                              children: [
                                                                const Text(
                                                                  "Pasajero - Destino",
                                                                  style: TextStyle(fontSize: 10.0, color: Colors.white),
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                  children: [
                                                                    Expanded(
                                                                      child: Row(
                                                                        children: [
                                                                          Image.asset(
                                                                            "images/distancia.png",
                                                                            height: 15.0,
                                                                            //width: 16.0,
                                                                          ),
                                                                          const SizedBox(
                                                                            width: 1,
                                                                          ),
                                                                          Expanded(
                                                                            child: Text(
                                                                              '${rideDetails.distanceText}',
                                                                              style: TextStyle(fontSize: 15.0, color: Colors.white),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    SizedBox(width: 2),
                                                                    Expanded(
                                                                      child: Row(
                                                                        children: [
                                                                          Image.asset(
                                                                            "images/tiempo.png",
                                                                            height: 15.0,
                                                                            //width: 16.0,
                                                                          ),
                                                                          const SizedBox(
                                                                            width: 1,
                                                                          ),
                                                                          Expanded(
                                                                            child: Text(
                                                                              '${rideDetails.durationText}',
                                                                              style: TextStyle(fontSize: 15.0, color: Colors.white),
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),

                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(width: 10),
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                          "images/pagar.png",
                                                          height: 25.0,
                                                          //width: 16.0,
                                                        ),
                                                        const SizedBox(
                                                          width: 1,
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            '${double.parse(rideDetails.tarifaOfertada).toStringAsFixed(2)} Bs.',
                                                            style: TextStyle(fontSize: 25.0, color: Colors.red, fontWeight: FontWeight.w500),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 3.0,
                                            ),
                                            Expanded(
                                              flex: 3,
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Column(
                                                      mainAxisSize: MainAxisSize.min,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        ///punto A
                                                        Expanded(
                                                          child: Row(
                                                            children: [
                                                              Image.asset(
                                                                "images/desticon.png",
                                                                height: 15.0,
                                                                //width: 16.0,
                                                              ),
                                                              const Text(
                                                                'A',
                                                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0, color: primary),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              Expanded(
                                                                child: Text(
                                                                  rideDetails.pickupAddress,
                                                                  style: Theme.of(context).textTheme.titleMedium,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 3.0,
                                            ),

                                            Expanded(
                                              flex: 3,
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Column(
                                                      mainAxisSize: MainAxisSize.min,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Expanded(
                                                          child: Row(
                                                            children: [
                                                              Image.asset(
                                                                "images/desticon2.png",
                                                                height: 15.0,
                                                                //width: 16.0,
                                                              ),
                                                              const Text(
                                                                'B',
                                                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0, color: Colors.blueAccent),
                                                              ),
                                                              const SizedBox(
                                                                width: 10,
                                                              ),
                                                              Expanded(
                                                                  child: Text(
                                                                rideDetails.dropoffAddress,
                                                                style: Theme.of(context).textTheme.titleMedium,
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),

                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              //child: Container(
                                //color: Colors.blue, // Cambia este color al que desees
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child: Column(
                                    //color: Colors.black,
                                    children: [

                                      /*conductor - pasajero*/
                                      Expanded(
                                        child: Row(
                                          children: [
                                            // Text(
                                            //   "•",
                                            //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.0),
                                            // ),
                                            ///conductor - pasajero
                                            Expanded(
                                              flex: 5,
                                              child: Container(
                                                padding: EdgeInsets.fromLTRB(10, 1, 1, 1),
                                                color: Colors.pink,
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    const Text(
                                                      "Conductor - Pasajero",
                                                      style: TextStyle(fontSize: 10.0, color: Colors.white),
                                                    ),
                                                    Expanded(
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: [
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: [
                                                              Image.asset(
                                                                "images/distancia.png",
                                                                height: 15.0,
                                                                //width: 16.0,
                                                              ),
                                                              const SizedBox(
                                                                width: 3,
                                                              ),
                                                              Row(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children:
                                                                    rideDetails.conductoresDistanciaTiempo.values.where((conductor) => conductor['conductor_code'] == driver.code).map((conductor) {
                                                                  return Text(
                                                                    '${conductor['distancia_conductor_pasajero']}',
                                                                    style: const TextStyle(fontSize: 15.0, color: Colors.white),
                                                                  );
                                                                }).toList(),
                                                              ),
                                                            ],
                                                          ),
                                                          SizedBox(width: 2),
                                                          Row(
                                                            children: [
                                                              Image.asset(
                                                                "images/tiempo.png",
                                                                height: 30.0,
                                                                //width: 16.0,
                                                              ),
                                                              const SizedBox(
                                                                width: 1,
                                                              ),
                                                              Row(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children:
                                                                    rideDetails.conductoresDistanciaTiempo.values.where((conductor) => conductor['conductor_code'] == driver.code).map((conductor) {
                                                                  return Text(
                                                                    '${conductor['tiempo_conductor_pasajero']}',
                                                                    style: const TextStyle(fontSize: 15.0, color: Colors.white),
                                                                  );
                                                                }).toList(),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),

                                            ///tipo carrera
                                            Expanded(
                                              flex: 4,
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    "Tipo carrera:",
                                                    style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Expanded(
                                                          child: Row(
                                                            children: [
                                                              Expanded(
                                                                child: Text(
                                                                  '${rideDetails.tipoCarrera}',
                                                                  style: Theme.of(context).textTheme.titleMedium,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        //SizedBox(width: 2),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              //),
                            ),

                            ///botones aceptar, precio, cancelar, acpetar tarifa.
                            Expanded(
                              flex: 4,
                              child: Container(
                                child: Column(
                                  children: [
                                    ///boton aceptar
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(5),
                                        child: ElevatedButton(
                                          onPressed: () async {
                                            acceptRideRequest(hash);
                                          },
                                          style: ElevatedButton.styleFrom(
                                            foregroundColor: Colors.white,
                                            backgroundColor: primary,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(18.0),
                                              side: BorderSide(color: Colors.green),
                                            ),
                                          ),
                                          // color: Colors.green,
                                          // textColor: Colors.white,
                                          child: Text(
                                            '${"Aceptar ".toUpperCase()}por: ${((tarifaSugerida)).toStringAsFixed(2)}Bs.',
                                            style: TextStyle(fontSize: 14, color: secundary),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Container(
                                          //height: 40,
                                          child: Column(
                                            children: [
                                              Expanded(
                                                flex: 2,

                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: <Widget>[
                                                      FloatingActionButton(
                                                        heroTag: "btn1",
                                                        onPressed: minus,
                                                        backgroundColor: Colors.white,
                                                        child: const Icon(
                                                          Icons.remove,
                                                          color: red,
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Text('Bs. ${(tarifaOfertada).toStringAsFixed(2)}', textAlign: TextAlign.center, style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.w500)),
                                                      ),
                                                      FloatingActionButton(
                                                        heroTag: "btn2",
                                                        onPressed: add,
                                                        backgroundColor: Colors.white,
                                                        child: const Icon(
                                                          Icons.add,
                                                          color: red,
                                                        ),
                                                      ),
                                                    ],
                                                  ),

                                              ),

                                              ///cancelar ofrecer tarifa
                                              SizedBox(height: 5.0,),
                                              Expanded(
                                                flex:1,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Expanded(
                                                      child: ElevatedButton(
                                                          child: Text('Cancelar'),
                                                          onPressed: _isButtonDisabledCancelar
                                                              ? null
                                                              : () async {
                                                                  await _handleCancelarButton('cancelar');
                                                                  estadoSubscription?.cancel();
                                                                },
                                                          onLongPress: () {
                                                            print('Long press');
                                                          },
                                                          style: ButtonStyle(
                                                            backgroundColor: MaterialStateProperty.all(Colors.red),
                                                          )),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Expanded(
                                                      child: ElevatedButton(
                                                          child: Text('Ofrecer tarifa'),
                                                          onPressed: _isButtonDisabled ? ofrecerTarifa : null,

                                                          // Deshabilitar el onPressed cuando _isButtonDisabled es true
                                                          style: ButtonStyle(
                                                            backgroundColor: MaterialStateProperty.all(_isButtonDisabled ? green : grey),
                                                            //padding: MaterialStateProperty.all(EdgeInsets.all(50)),
                                                            //textStyle: MaterialStateProperty.all(TextStyle(fontSize: 5))),
                                                          )),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                : isVisiblePositioned2
                    ? Positioned(
                        left: 0.0,
                        right: 0.0,
                        bottom: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black38,
                                blurRadius: 16.0,
                                spreadRadius: 0.5,
                                offset: Offset(0.7, 0.7),
                              ),
                            ],
                          ),
                          height: 175.0, // Ajusta la altura según tus necesidades
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 15, 20, 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Row(
                                        children: [
                                          Container(
                                            height: 30,
                                            width: 30,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(15),
                                              color: secundary,
                                            ),
                                            child: Image.asset(
                                              "images/pasajero.png",
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                rideDetails.riderName,
                                                style: TextStyle(fontFamily: "Brand-Bold", fontSize: 18.0),
                                              ),
                                              Text(
                                                'Carreras Tomadas: 5',
                                                style: TextStyle(fontSize: 12.0, color: Colors.grey),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    ///origen
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Origen:',
                                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Text("Detalles del origen"),
                                                    content: Text(rideDetails.pickupAddress),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.pop(context);
                                                        },
                                                        child: Text("Cerrar"),
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                            child: Text(
                                              widget.rideDetails.dropoffAddress,
                                              style: TextStyle(fontSize: 16.0),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    ///boton stado carrera
                                    Container(
                                      height: 40,
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          backgroundColor: btnColor, // Cambia el color aquí
                                          // Puedes ajustar otros estilos según tus necesidades
                                        ),
                                        onPressed: () async {
                                          if (statusRide == "aceptada") {
                                            statusRide = "arrived";
                                            _historialCollection.doc(hash).update({'status': statusRide});

                                            setState(() {
                                              btnTitle = "Iniciar viaje";
                                              verMapa = true;
                                              btnColor = secundary;
                                            });

                                            p2 = LatLng(double.parse(rideDetails.dropOffLocMap['latitude'].toString()), double.parse(rideDetails.dropOffLocMap['longitude'].toString()));
                                            showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) => ProgressDialog(message: "Por favor espere"),
                                            );

                                            //await getPlaceDirection(widget.rideDetails.pickup, widget.rideDetails.dropoff);
                                            Navigator.pop(context);
                                          } else if (statusRide == "arrived") {
                                            statusRide = "onride";
                                            _historialCollection.doc(hash).update({'status': statusRide});
                                            //newRequestsRef.child(rideRequestId).child("status").set(status);

                                            setState(() {
                                              btnTitle = "Fin de viaje";
                                              btnColor = red;
                                            });

                                            initTimer();
                                          } else if (statusRide == "onride") {
                                            _stopPositionStream();
                                            await endTheTrip();
                                            rideRequestServices.stopListeningToTarifaSugerida();
                                            statusRide = "finished";
                                            _historialCollection.doc(hash).update({'status': statusRide});
                                            Navigator.pop(context);
                                            Navigator.pop(context, 'finished');
                                            //banderaCarreraPendiente = false;
                                          }
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              btnTitle,
                                              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.white),
                                            ),
                                            const SizedBox(width: 8.0),
                                            Icon(
                                              FontAwesomeIcons.taxi,
                                              color: Colors.white,
                                              size: 20.0,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Bs. ' + rideDetails.precioAceptado.toStringAsFixed(2),
                                            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.red),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Row(
                                        children: [
                                          Text(
                                            'Deseo:',
                                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey),
                                          ),
                                          Text(
                                            rideDetails.deseo,
                                            style: TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    ///destino
                                    Expanded(
                                      flex: 2,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Destino:',
                                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Text("Detalles del destino"),
                                                    content: Text(widget.rideDetails.dropoffAddress),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.pop(context);
                                                        },
                                                        child: Text("Cerrar"),
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                            child: Text(
                                              widget.rideDetails.dropoffAddress,
                                              style: TextStyle(fontSize: 16.0),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    ///distancia
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Distancia:',
                                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.grey),
                                          ),
                                          Text(
                                            widget.rideDetails.distanceText,
                                            style: TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Container(),
          ],
        ),
      ),
      onWillPop: () async {
        if (isBack) {
          await _handleCancelarButton('');
          return true; // Permite que el retroceso se produzca
        } else {
          return false;
        }
      },
    );
  }

  void detenerListener() {
    if (_rideRequestSubscription != null) {
      _rideRequestSubscription!.cancel();
      _rideRequestSubscription = null; // Limpia la referencia del oyente
    }
  }

  ofrecerTarifa() async {
    setState(() {
      _isButtonDisabled = false;
    });
    DocumentReference rideRequestRef = _rideRequestsCollection.doc(hash);
    DocumentSnapshot snapshot = await rideRequestRef.get();

    if (snapshot.exists) {
      Map<String, dynamic> conductoresDistanciaTiempo = rideDetails.conductoresDistanciaTiempo;
      String tiempoConductorPasajero = '';
      // Buscar el objeto ConductorDistanciaTiempo correspondiente al conductor actual
      var conductorDistanciaTiempo;
      conductoresDistanciaTiempo.forEach((conductorKey, conductorValue) {
        if (conductorValue['conductor_code'] == driver.code) {
          conductorDistanciaTiempo = conductorValue;
          return;
        }
      });

      if (conductorDistanciaTiempo != null) {
        tiempoConductorPasajero = conductorDistanciaTiempo['tiempo_conductor_pasajero'];
      }

      // Crear el mapa del conductor para la oferta de tarifa
      Map<String, dynamic> conductor = {
        'conductor_id': currentfirebaseUser.uid,
        'tarifa_ofertada': tarifaOfertada,
        'conductor_color_vehiculo': driver.carColor,
        'conductor_modelo_vehiculo': driver.carModel,
        'conductor_nombre': driver.name,
        'tiempo_estimado': tiempoConductorPasajero,
        'creation_time': DateTime.now().toUtc().toString(), // Agrega la hora actual en formato UTC
        'duration_in_seconds': 10
      };

      Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
      // Obtén la lista de conductores actuales que han ofrecido tarifas
      List<dynamic> conductores = data['rideInfoMap']['conductores'] ?? [];

      // Verifica si ya hay 7 conductores que han ofrecido tarifas
      if (conductores.length >= 7) {
        print('Ya hay 7 conductores que han ofrecido tarifas. No puedes ofertar.');
        return; // Sale de la función sin ofertar tarifa
      }

      bool foundConductor = false;
      int indexConductor = 0;
      for (int i = 0; i < conductores.length; i++) {
        if (conductores[i]['conductor_id'] == currentfirebaseUser.uid) {
          foundConductor = true;
          indexConductor = i;
          break;
        }
      }

      if (foundConductor) {
        // Si ya existe un conductor con la misma propiedad "conductor_id", actualizamos su propiedad "tarifa_ofertada"
        conductores[indexConductor]['tarifa_ofertada'] = tarifaOfertada;
      } else {
        // Si no existe un conductor con la misma propiedad "conductor_id", agregamos el nuevo conductor
        conductores.add(conductor);
      }

      // Actualizamos el campo "conductores" en la solicitud de viaje
      await _rideRequestsCollection.doc(hash).update({'rideInfoMap.conductores': conductores});
      print(conductores);
      // Agrega un observador al campo de aceptación para escuchar cambios
      _rideRequestSubscription = _rideRequestsCollection.doc(hash).snapshots().listen((snapshot) {
        print('escuchando _rideRequestsCollection');
        if (snapshot.exists) {
          Map<String, dynamic> rideInfoMap = snapshot['rideInfoMap'] as Map<String, dynamic>;
          print('rideInfoMap1');
          print(rideInfoMap);
          String estado = rideInfoMap['status'] ?? '';

          if (estado == 'aceptada') {
            print('rideInfoMap2');
            print(rideInfoMap);
            // Si el pasajero acepta la oferta, cancelar el timer
            if (_timerOffer != null && _timerOffer!.isActive) {
              _timerOffer!.cancel();
              _timerOffer = null; // Limpiar la variable del timer
            }

            isBack = false;
            String conductorIdOfertaAceptada = rideInfoMap['driver_id'] ?? '';

            if (conductorIdOfertaAceptada == driver.id) {
              // Solo el conductor cuya oferta fue aceptada debe continuar con el flujo normal de la carrera
              print('Tu oferta de tarifa fue aceptada. Continúa con el flujo normal de la carrera.');
              detenerListener();
              //print('play1');
              // Realiza las acciones necesarias para el conductor cuya oferta fue aceptada
              ofertaAceptada(hash);
            } else {
              // El conductor no tiene que hacer nada ya que su oferta no fue aceptada
              print('Tu oferta de tarifa no fue aceptada.');
              detenerListener();
              _handleCancelarButton('oferta_rechazada');
              //_timerOffer.cancel();
            }
          } else {
            // El estado no es "aceptada", puedes manejarlo aquí si es necesario
          }
        }
      });

      // Configura un Timer para que se active después de 10 segundos
      print('isBotActive');
      print(isBotActive);
      if (isBotActive) {
        _timerOffer = Timer(Duration(seconds: 10), () async {
          print('timer iniciado');
          detenerListener();
          _handleCancelarButton('oferta_rechazada');
        });
      }
    } else {
      //volver_atras
      print('onPress ofrecer tarifa');
      _handleCancelarButton('oferta_rechazada');
    }
  }

  Future<void> playAAC() async {
    AudioPlayer audioPlayer = AudioPlayer();
    await audioPlayer.play(AssetSource('../sounds/nueva_solicitud.aac'), mode: PlayerMode.mediaPlayer);
  }

  Future<void> _handleCancelarButton(String? ofertaRechazada) async {
    rideRequestServices.stopListeningToTarifaSugerida();
    _isButtonDisabledCancelar = true;

    DocumentReference rideRequestRef = _rideRequestsCollection.doc(hash);
    DocumentSnapshot snapshot = await rideRequestRef.get();

    if (snapshot.exists) {
      Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
      List<dynamic> conductores = data['rideInfoMap']['conductores'] ?? [];

      bool foundConductor = false;
      int indexConductor = 0;
      for (int i = 0; i < conductores.length; i++) {
        if (conductores[i]['conductor_id'] == currentfirebaseUser.uid) {
          foundConductor = true;
          indexConductor = i;
          break;
        }
      }

      if (foundConductor) {
        conductores.removeAt(indexConductor);
      } else {}

      await _rideRequestsCollection.doc(hash).update({'rideInfoMap.conductores': conductores});
    }

    Navigator.pop(context, ofertaRechazada);
  }

  Future<void> endTheTrip() async {
    //driversRef.child(currentfirebaseUser.uid).child('key_ultima_carrera').remove();
    //timer.cancel();
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => ProgressDialog(
        message: "Por favor espere",
      ),
    );

    //obtener tipo vehiculo para calcular tarifa

    final historyRef = _historialCollection.doc(hash);
    final historySnapshot = await historyRef.get();
    final data = historySnapshot.data() as Map<String, dynamic>?;
    final rideInfoMap = data?['rideInfoMap'] as Map<String, dynamic>?;

    //***CALCULO TARIFA***//
    double fareAmount = (rideInfoMap?['precio_aceptado']);
    var result = '';
    if (rideInfoMap?['tipo_carrera'] == 'normal') {
      result = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => CollectFareDialog(paymentMethod: "cash", fareAmount: fareAmount),
      );
    } else {
      result = 'close';
    }
    // Do something with the result
    if (result == "close") {
      final result = await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => RatingScreen(
                driverId: rideInfoMap?["rider_id"],
              )));
      if (result == "close") {
        //resetApp();
        print("bandera4");
        //actualizo historial
        final driverRef = FirebaseFirestore.instance.collection('drivers').doc(currentfirebaseUser.uid);
        final driverSnapshot = await driverRef.get();
        final Map<String, dynamic> userRides = Map<String, dynamic>.from(driverSnapshot.data()?['rides'] ?? {});
        userRides[hash] = true; // Donde rideId es el ID de la carrera que deseas actualizar
        driverRef.update({'rides': userRides});
        //_positionStreamSubscription.cancel();
        //_stopPositionStream();
      }
    } else {}
    int conteo = Provider.of<AppData>(context, listen: false).countTrips;
    print('conteo');
    print(conteo);
    Provider.of<AppData>(context, listen: false).updateCountTrips(conteo + 1);
    //rideInfoMap!['driver_code'] = datosConductor?.code.toString();
    Provider.of<AppData>(context, listen: false).updateTripKey(hash);
    Provider.of<AppData>(context, listen: false).updateTripHistoryData(HistoryModel.fromMap(rideInfoMap!));
    print(Provider.of<AppData>(context, listen: false).countTrips);

    saveEarnings(fareAmount);
  }

  Future<void> saveEarnings(double fareAmount) async {
    final userRef = FirebaseFirestore.instance.collection('drivers').doc(currentfirebaseUser.uid);

    userRef.get().then((docSnapshot) {
      if (docSnapshot.exists) {
        String earningsStr = docSnapshot.data()?['earnings'] ?? '0';
        double oldEarnings = double.parse(earningsStr);
        double totalEarnings = fareAmount + oldEarnings;

        userRef.update({'earnings': totalEarnings.toStringAsFixed(2)});
      } else {
        double totalEarnings = fareAmount.toDouble();
        userRef.set({'earnings': totalEarnings.toStringAsFixed(2)});
      }
    });
  }

  void initTimer() {
    const interval = Duration(seconds: 1);
    timer = Timer.periodic(interval, (timer) {
      durationCounter = durationCounter + 1;
    });
  }

  Future<void> acceptRideRequest(String rideId) async {
    final rideRef = FirebaseFirestore.instance.collection('rideRequests').doc(rideId);

    try {
      final datosCarrera = await rideRef.get();

      if (datosCarrera.exists) {
        String status = datosCarrera.data()?['rideInfoMap']['status'];

        if (status == 'solicitud_enviada') {
          await FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
            var datosCarrera = await transaction.get(rideRef);

            if (!datosCarrera.exists) {
              throw Exception('La carrera ya fue tomada o no está disponible.'); // No existe el documento, no se puede procesar la transacción
            }

            String status = datosCarrera.data()?['rideInfoMap']['status'];

            if (status == 'aceptada') {
              Fluttertoast.showToast(
                msg: "La carrera ha sido cancelada y ya no está disponible.",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                backgroundColor: Colors.red,
                textColor: Colors.white,
              );
              throw Exception('La carrera ya fue tomada o no está disponible.');
            }

            // Si el pasajero acepta la oferta, cancelar el timer
            if (_timerOffer != null && _timerOffer!.isActive) {
              _timerOffer!.cancel();
              _timerOffer = null; // Limpiar la variable del timer
            }

            // Actualizar el estado de la carrera a "aceptada"
            await transaction.update(rideRef, {
              'rideInfoMap.status': 'aceptada',
              'rideInfoMap.driver_id': currentfirebaseUser.uid,
              'rideInfoMap.precio_aceptado': tarifaSugerida,
            });

            statusRide = 'aceptada';
          });
          print('procesar aceptacion');
          processAcceptedRide(rideId);
        } else {
          throw Exception('La carrera ya fue tomada o no está disponible.');
        }
      } else {
        throw Exception('El documento no existe.');
      }
    } catch (e) {
      print('Error al tomar la carrera: $e');
      // Manejar el error, por ejemplo, notificar al conductor que la carrera ya fue tomada.
      Fluttertoast.showToast(
        msg: "La carrera ha sido cancelada y ya no está disponible.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
      await Future.delayed(Duration(seconds: 5), () async {
        Navigator.pop(context, "oferta_rechazada");
      });
    }
  }

  Future<void> ofertaAceptada(String rideId) async {
    // Llamar a la función común para procesar la aceptación
    await processAcceptedRide(rideId);
  }

  Future<void> processAcceptedRide(String rideId) async {
    setState(() {
      isBack = false;
    });
    final rideRef = FirebaseFirestore.instance.collection('rideRequests').doc(rideId);

    // Verificar si la carrera aún existe antes de continuar
    final datosCarrera = await rideRef.get();

    final historyRef = FirebaseFirestore.instance.collection('historial').doc(rideId);
    final rideInfoMap = datosCarrera.data()?['rideInfoMap'] as Map<String, dynamic>?;
    print(datosCarrera.data());
    if (rideInfoMap != null) {
      // Copiar la solicitud de viaje en la colección "historial" con el campo 'status' como 'aceptada'
      await historyRef.set({
        'rideInfoMap': rideInfoMap, // Mantener los datos originales
        'status': 'aceptada', // Agregar el campo 'status' con el valor 'aceptada'
      });

      final driverRef = FirebaseFirestore.instance.collection('drivers').doc(currentfirebaseUser.uid);
      final driverSnapshot = await driverRef.get();

      // Borrar carrera de rideRequest
      await rideRef.delete();

      final userRides = Map<String, dynamic>.from(driverSnapshot.data()?['rides'] ?? {});

      // Agregar la nueva carrera al Map de carreras
      userRides[rideId] = false; // donde docId es el ID de la solicitud de viaje que se acaba de enviar

      // Guardar el nuevo Map de carreras en el documento del usuario
      driverRef.update({'rides': userRides});
      _initPositionStream('historial');
      setState(() {
        isVisiblePositioned1 = false;
        isVisiblePositioned2 = true;
        statusRide = 'aceptada';
      });
      print('play2');
      playAAC();
    } else {
      print('rideInfo es null');

      if (widget.makeOffer != null) {
        print('rideInfoMap');
        final driverRef = FirebaseFirestore.instance.collection('drivers').doc(currentfirebaseUser.uid);
        final driverSnapshot = await driverRef.get();
        final userRides = Map<String, dynamic>.from(driverSnapshot.data()?['rides'] ?? {});
        userRides[rideId] = false; // donde docId es el ID de la solicitud de viaje que se acaba de enviar
        driverRef.update({'rides': userRides});
        _initPositionStream('historial');
        setState(() {
          isVisiblePositioned1 = false;
          isVisiblePositioned2 = true;
          statusRide = 'aceptada';
        });
      }
      print('No se encontró rideInfoMap en los datos de la carrera.');
    }
  }

  void _initPositionStream(String database) {
    const locationOptions = LocationSettings(
      accuracy: LocationAccuracy.bestForNavigation,
      distanceFilter: 0,
    );
    print("inicio positionStreamSubscription");
    _positionStreamSubscription = _geolocator.getPositionStream(locationSettings: locationOptions).listen(
      (Position position) async {
        print('escuchando posicion driver');
        LatLng mPostion = LatLng(position.latitude, position.longitude);
        currentPosition = position;
        var rot = MapKitAssistant.getMarkerRotation(
          _previousLocation?.latitude ?? 0.toDouble(),
          _previousLocation?.longitude ?? 0.toDouble(),
          position.latitude,
          position.longitude,
        );
        Marker animatingMarker = Marker(
          markerId: MarkerId("animating"),
          position: mPostion,
          icon: customIcon!,
          rotation: rot,
          infoWindow: InfoWindow(title: "Current Location"),
        );
        if (mounted) {
          setState(() {
            CameraPosition cameraPosition = new CameraPosition(target: mPostion, zoom: 17);
            if (mapController != null) {
              mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
            }

            _markers.removeWhere((marker) => marker.markerId.value == "animating");
            _markers.add(animatingMarker);
          });
        }

        final currentTime = DateTime.now();
        GeoFirePoint center = geo.point(latitude: position.latitude, longitude: position.longitude);
        await _updateDriverPosition(center, database, currentTime);
        final distanceInMetros = _calculateDistance(mPostion, p2);
        final distanciaInKilometros = distanceInMetros / 1000;
        if (mounted) {
          setState(() {
            distanceTextConductorPasajero = distanciaInKilometros.toStringAsFixed(2) + " km";
          });
        }
      },
    );
    isPositionStreamSubscription = true;
  }

  Future<bool> checkRideExists(String rideId) async {
    final rideRef = FirebaseFirestore.instance.collection('rideRequests').doc(rideId);
    final docSnapshot = await rideRef.get();
    return docSnapshot.exists;
  }

  Future<void> calculateRoute() async {
    String tipoCarrera = rideDetails.tipoCarrera;
    Position posicionConductor = widget.currrentPosition;
    LatLng posicionPasajero = LatLng(double.parse(rideDetails.pickUpLocMap['latitude']!), double.parse(rideDetails.pickUpLocMap['longitude']!));
    Position posicionPasajeroP = Position(
      longitude: posicionPasajero.longitude,
      latitude: posicionPasajero.latitude,
      timestamp: DateTime.now(),
      accuracy: 5.0,
      altitude: 100.0,
      heading: 0.0,
      speed: 0.0,
      speedAccuracy: 0.0,
    );
    LatLng posicionDestino = LatLng(double.parse(rideDetails.dropOffLocMap['latitude']!), double.parse(rideDetails.dropOffLocMap['longitude']!));

    DirectionDetails details = await AssistantMehods.obtainPlaceDirectionDetails(posicionConductor, posicionPasajero);
    DirectionDetails? details1;
    if (tipoCarrera != 'taxi_a_la_ubicacion') {
      details1 = await AssistantMehods.obtainPlaceDirectionDetails(posicionPasajeroP, posicionDestino);
    }

    distanceTextOrigenDestino = details1?.distanceText ?? '';
    durationTextOrigenDestino = details1?.durationText ?? '';

    LatLng startPosition = LatLng(posicionConductor.latitude, posicionConductor.longitude);
    LatLng passengerPosition = posicionPasajero;
    LatLng destinationPosition = posicionDestino;

    setState(() {
      _markers.add(Marker(
          markerId: MarkerId('conductor'),
          position: startPosition,
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          infoWindow: InfoWindow(
            title: "Conductor",
            snippet: 'Esta es mi ubicacion',
          )));

      _markers.add(Marker(
        markerId: MarkerId('pasajero'),
        position: passengerPosition,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        infoWindow: InfoWindow(
          title: 'Pasajero',
          snippet: 'Esta es mi ubicacion',
          onTap: () {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Pasajero'),
                content: Text('Esta es mi ubicacion'),
              ),
            );
          },
        ),
      ));

      _markers.add(Marker(
        markerId: MarkerId('destino'),
        position: destinationPosition,
        icon: BitmapDescriptor.defaultMarkerWithHue(190.0),
        infoWindow: InfoWindow(
          title: "Destino",
          snippet: 'Esta es mi ubicacion',
        ),
      ));
    });

    PolylinePoints polylinePoints = PolylinePoints();
    List<PointLatLng> decodePolyLinePointsResult = polylinePoints.decodePolyline(details.encodePoints);
    List<LatLng> polylineCoordinates = [];
    if (decodePolyLinePointsResult.isNotEmpty) {
      decodePolyLinePointsResult.forEach((PointLatLng pointLatlng) {
        polylineCoordinates.add(LatLng(pointLatlng.latitude, pointLatlng.longitude));
      });
    }

    Polyline polyline = Polyline(
      color: Colors.red,
      polylineId: PolylineId("Polyline"),
      jointType: JointType.round,
      points: polylineCoordinates,
      width: 5,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      geodesic: true,
    );

    if (tipoCarrera != 'taxi_a_la_ubicacion') {
      PolylinePoints polylinePoints1 = PolylinePoints();
      List<PointLatLng> decodePolyLinePointsResult1 = polylinePoints1.decodePolyline(details1!.encodePoints);
      List<LatLng> polylineCoordinates1 = [];
      if (decodePolyLinePointsResult1.isNotEmpty) {
        decodePolyLinePointsResult1.forEach((PointLatLng pointLatlng) {
          polylineCoordinates1.add(LatLng(pointLatlng.latitude, pointLatlng.longitude));
        });
      }

      Polyline polyline1 = Polyline(
        color: Colors.blueAccent,
        polylineId: PolylineId("Polyline1"),
        jointType: JointType.round,
        points: polylineCoordinates1,
        width: 5,
        startCap: Cap.roundCap,
        endCap: Cap.roundCap,
        geodesic: true,
      );

      setState(() {
        polYlineSet.clear();
        polYlineSet.add(polyline);
        polYlineSet.add(polyline1);
      });
    } else {
      setState(() {
        polYlineSet.clear();
        polYlineSet.add(polyline);
      });
    }

    // Crear una lista de LatLng que representan los puntos de la ruta
  }

  void add() {
    setState(() {
      _isButtonDisabled = true;
      tarifaOfertada++;
    });
  }

  void minus() {
    if (tarifaOfertada > double.parse(rideDetails.tarifaSugerida)) {
      setState(() {
        if (tarifaOfertada != 0) _isButtonDisabled = true;
        tarifaOfertada--;
      });
    } else {
      displayToastMessage('Tarifa no puede ser menor que tarifa sugerida', context);
    }
  }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }

  double _calculateDistance(LatLng p1, LatLng p2) {
    const int earthRadius = 6371000; // meters
    double lat1 = p1.latitude;
    double lat2 = p2.latitude;
    double lon1 = p1.longitude;
    double lon2 = p2.longitude;
    double dLat = (lat2 - lat1) * Math.pi / 180;
    double dLon = (lon2 - lon1) * Math.pi / 180;
    double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * Math.pi / 180) * Math.cos(lat2 * Math.pi / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = earthRadius * c;
    return distance;
  }

  Future<void> _updateDriverPosition(GeoFirePoint center, String database, DateTime timestamp) async {
    DocumentReference driverRef = FirebaseFirestore.instance.collection(database).doc(hash);
    bool docExists = await driverRef.get().then((doc) => doc.exists);
    if (docExists) {
      await driverRef.update({
        'position': center.data,
        'timestamp': timestamp.toUtc(),
      });
    } else {
      print("no existe");
      await driverRef.set({
        'position': center.data,
        'timestamp': timestamp.toUtc(),
      });
    }
  }

  void _openMap() async {
    String pickup = rideDetails.pickUpLocMap['latitude'].toString() + ',' + rideDetails.pickUpLocMap['longitude'].toString();
    String dropoff = rideDetails.dropOffLocMap['latitude'].toString() + ',' + rideDetails.dropOffLocMap['longitude'].toString();
    if (statusRide == 'aceptada') {
      print('aceptada');
      dropoff = pickup;
      pickup = currentPosition.latitude.toString() + ',' + currentPosition.longitude.toString();
    }

    String url = 'https://www.google.com/maps/dir/?api=1&origin=' + pickup + '&destination=' + dropoff + '&travelmode=driving';
    print('open map');
    print(url);
    final Uri _url = Uri.parse(url);

    if (await canLaunch(_url.toString())) {
      // Usar la URL personalizada para abrir Google Maps
      await launch(_url.toString());
    } else {
      // Si no se puede abrir Google Maps, mostrar un mensaje de error
      throw 'Could not launch Google Maps';
    }
  }

  void _openMapTaxiALaUbicacionRecogerPasajero() async {
    String dropoff = rideDetails.pickUpLocMap['latitude'].toString() + ',' + rideDetails.pickUpLocMap['longitude'].toString();
    String pickup = currentPosition.latitude.toString() + ',' + currentPosition.longitude.toString();

    String url = 'https://www.google.com/maps/dir/?api=1&origin=' + pickup + '&destination=' + dropoff + '&travelmode=driving';
    final Uri _url = Uri.parse(url);

    if (await canLaunch(_url.toString())) {
      await launch(_url.toString());
    } else {
      throw 'Could not launch $url';
    }
  }

  void _openMapTaxiALaUbicacion() async {
    String dropoff = rideDetails.pickUpLocMap['latitude'].toString() + ',' + rideDetails.pickUpLocMap['longitude'].toString();
    String pickup = rideDetails.dropOffLocMap['latitude'].toString() + ',' + rideDetails.dropOffLocMap['longitude'].toString();

    String url = 'https://www.google.com/maps/dir/?api=1&origin=' + pickup + '&destination=' + dropoff + '&travelmode=driving';

    final Uri _url = Uri.parse(url);

    if (await canLaunch(_url.toString())) {
      await launch(_url.toString());
    } else {
      throw 'Could not launch $url';
    }
  }

  void openwhatsapp(String mensaje) async {
    print(mensaje);
    String whatsapp = "+591" + rideDetails.riderPhone;

    var whatsappURl_android = "whatsapp://send?phone=" + whatsapp + "&text=" + mensaje;
    var whatappURL_ios = "https://wa.me/$whatsapp?text=${Uri.parse("hello")}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunchUrl(Uri.parse(whatappURL_ios))) {
        await launchUrl(
          Uri.parse(whatappURL_ios),
          mode: LaunchMode.externalApplication,
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("whatsapp no instalado")));
      }
    } else {
      // android , web
      if (await canLaunchUrl(Uri.parse(whatsappURl_android))) {
        await launchUrl(
          Uri.parse(whatsappURl_android),
          mode: LaunchMode.externalApplication,
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("whatsapp no instalado")));
      }
    }
  }
}
