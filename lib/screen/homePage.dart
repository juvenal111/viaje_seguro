import 'dart:async';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:switcher/core/switcher_size.dart';
import 'package:switcher/switcher.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zmovil_conductor/assistant/authRepository.dart';
import 'package:zmovil_conductor/helpers/style.dart';
import 'package:zmovil_conductor/model/zonaTrabajo.dart';
import 'package:zmovil_conductor/widget/requestItem.dart';
import '../Observer/rides_observer.dart';
import '../assistant/assistantMethods.dart';
import '../assistant/driverRepository.dart';
import '../assistant/jobRepository.dart';
import '../helpers/UpdateChecker.dart';
import '../helpers/constants.dart';
import '../model/botDriverConfig.dart';
import '../model/driver.dart';
import '../model/rideRequests.dart';
import '../provider/botDriver.dart';
import '../provider/driver.dart';
import '../provider/user.dart';
import '../services/botDriver.dart';
import '../variables_constantes.dart';
import '../widget/lockableSwitcher.dart';
import '../widget/menu.dart';
import '../widget/myCustomAppBat.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'carInfoScreen.dart';
import 'detalleCarreraPage.dart';

class HomePage extends StatefulWidget {
  static const String idScreen = "homePage";

  const HomePage({super.key});

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late AuthRepository authRepository = AuthRepository();

  late JobRepository jobRepository;
  bool _isFree = false;
  late Geoflutterfire geo;

  late Stream<List<DocumentSnapshot>> stream;

  late StreamSubscription<List<DocumentSnapshot>> _ridesSubscription; // nuevo miembro de la clase

  final GeolocatorPlatform _geolocator = GeolocatorPlatform.instance;
  late StreamSubscription<Position> _positionStreamSubscription;

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  late Position currentPosition;
  final radius = 15.0; // en km

  late DriverRepository driverRepository;
  late StreamSubscription<Driver?> driverSubscription; // Declara una variable para el listener
  late Driver currentDriver;

  /*resetear*/
  bool isInitializedPositionStreamSubscription = false;
  bool isInitializedRideSubscription = false;
  bool isBotBusy = false;
  bool shouldContinueBot = true; // Inicialmente, el bot debe continuar ejecutándose

  StreamSubscription<Driver>? _driverStreamSubscription;
  List<String> offeredRideIds = [];
  List<RideRequest> carrerasDisponibles = [];
  List<String> carrerasTomadas = [];

  final RidesObserver _ridesObserver = RidesObserver();
  bool tookInitialRides = false;
  bool isSwitchLocked = false; // Estado para habilitar o deshabilitar el Switch
  bool hasPaidBot = false;
  BotDriverService serviceBot = BotDriverService();
  late BotDriverConfig configBot;
  bool botBusy = false;
  final FirebaseAuth _auth = FirebaseAuth.instance;



  @override
  void initState() {
    super.initState();
    checkForUpdates(); // Verificar actualizaciones al cargar la vista
    driverRepository = DriverRepository();
    User? user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      driverSubscription = driverRepository.driver.listen((driver) {
        if (driver != null) {
          setState(() {
            currentDriver = driver;
          });
        }
      }, onError: (error) {
        print(error);
      }, onDone: () {
        print('Stream closed');
      });
    }
    stream = Stream.empty();
    currentfirebaseUser = user!;
    jobRepository = JobRepository();
    geo = Geoflutterfire();
    init();

    //_ridesObserver.addListener(_onRidesUpdated);
  }

  Future<void> checkForUpdates() async {
    // Lógica para verificar actualizaciones
    bool needsUpdate = await UpdateChecker.checkForUpdates();
    if (needsUpdate) {
      showUpdateDialog(); // Mostrar diálogo de actualización
    }
    print(needsUpdate);
  }
  void showUpdateDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Actualización disponible'),
          content: Text('Hay una nueva versión disponible. ¿Deseas actualizar?'),
          actions: <Widget>[
            TextButton(
              child: Text('Actualizar'),
              onPressed: () {
                Navigator.pop(context);
                launchAppStore(); // Abrir la tienda de aplicaciones para actualizar
              },
            ),
          ],
        );
      },
    );
  }

  void launchAppStore() async {
    const url = 'https://play.google.com/store/apps/details?id=sinet.startup.inDriver&hl=es_BO&gl=US'; // Reemplaza con la URL de tu tienda de aplicaciones

    if (await canLaunch(url)) {
      await launch(url);
      SystemNavigator.pop(); // Cerrar la aplicación después de redirigir a la tienda
    } else {
      print('No se pudo abrir la URL');
    }
  }

  Future<void> init() async {
    String? userId = _auth.currentUser?.uid;
    if (userId != null) {
      Timer(const Duration(seconds: 5), () async {
        bool isCheckInfoCar = await Provider.of<DriverProvider>(context, listen: false).hasFilledCarInfo(userId);
        if (!isCheckInfoCar) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => CarInfoScreen(user: _auth.currentUser!, isfromSplash: true),
            ),
          );
        }
      });
    }

    var configData = await serviceBot.getBotDriverConfig(currentfirebaseUser.uid);
    if (configData != null) {
      configBot = configData;
    } else {
      // Manejar el caso en que configData es nulo, por ejemplo, lanzar un error o asignar un valor predeterminado.
    }

    Position position = await _getLocation();
    currentPosition = position;
    getUserRides();
    bool estado = await driverRepository.getEstado(currentfirebaseUser.uid);
    print('status: ' + estado.toString());
    if (estado) {
      setState(() {
        _isFree = true;
      });
    }
    driverRepository.getHasPaidBot(currentfirebaseUser.uid).listen((event) {
      bool previusBool = hasPaidBot;
      hasPaidBot = event;
      if (previusBool && !hasPaidBot) {
        isBotActive = false;
      }
    });

    AssistantMehods.getCarrerasTomadas(context, currentfirebaseUser.uid);
  }

  @override
  void dispose() {
    _ridesObserver.cancel();
    _driverStreamSubscription?.cancel();
    if (isInitializedRideSubscription) {
      print('cancelar ridesSuscripcion');
      _ridesSubscription.cancel();
    }

    driverSubscription.cancel();
    //driverStream.
    stopStreamSubscription();

    super.dispose();
  }

  Future<void> getUserRides() async {
    final driverRef = FirebaseFirestore.instance.collection('drivers').doc(auth.currentUser!.uid);
    final userSnapshot = await driverRef.get();
    final allUserRides = Map<String, dynamic>.from(userSnapshot.data()?['rides'] ?? {});
    final falseRides = allUserRides.entries.where((entry) => entry.value == false).map((entry) => entry.key).toList();
    if (!falseRides.isEmpty) {
      String canceledRideId = falseRides.first;
      // Obtener la referencia al documento del viaje cancelado en la colección "historial"
      final canceledRideRef = FirebaseFirestore.instance.collection('historial').doc(canceledRideId);
      final canceledRideSnapshot = await canceledRideRef.get();

      // Verificar si el documento existe y obtener sus datos
      if (canceledRideSnapshot.exists) {
        Map<String, dynamic>? canceledRideData = canceledRideSnapshot.data();
        // canceledRideData!['conductores'] = [];
        // print('Datos de lar: $canceledRideData');
        RideRequest rideRequest = RideRequest.fromMap(canceledRideData!['rideInfoMap']);
        if (_isFree) {
          stopStreamSubscription();
          setState(() {
            _isFree = false;
          });
        }
        isInitializedPositionStreamSubscription = true;
        var res = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetalleCarreraScreen(
                      rideDetails: rideRequest,
                      hash: canceledRideId,
                      driver: currentDriver,
                      currrentPosition: currentPosition,
                      history: canceledRideSnapshot,
                      stopRidesSubscription: stopStreamSubscription,
                    )));
        isInitializedRideSubscription = true;
        resetRides();
        // if (res == true) {
        //   SwiilableDriver();
        //   print("detalle carrera devuelve:" + res.toString());
        // }
      }
    }

  }
  Future<Position> _getLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
    return position;
  }

  void _initPositionStream(String database) {
    print('escuchando position conductor');
    const locationOptions = LocationSettings(
      accuracy: LocationAccuracy.bestForNavigation,
      distanceFilter: 0,
    );
    _positionStreamSubscription = _geolocator.getPositionStream(locationSettings: locationOptions).listen(
      (Position position) {
        print('position driverrr');
        setState(() {
          currentPosition = position;
        });
        if (_isFree) {
          _updateDriverPosition(geo.point(latitude: position.latitude, longitude: position.longitude), database);
        }
      },
    );
    isInitializedPositionStreamSubscription = true;
  }

  Future<void> _updateDriverPosition(GeoFirePoint center, String database) async {
    DocumentReference driverRef = _firestore.collection(database).doc(currentfirebaseUser.uid);
    bool docExists = await driverRef.get().then((doc) => doc.exists);
    if (docExists) {
      await driverRef.update({'position': center.data});
    } else {
      await driverRef.set({'position': center.data});
    }
  }

  void _stopPositionStream() {
    print('detener position');
    if (isInitializedPositionStreamSubscription) {
      print('detener position true');
      //_positionStreamSubscription.cancel();
      _positionStreamSubscription.pause();
      isInitializedPositionStreamSubscription=false;
    }
  }

  void _signOutAndNavigateToLoginPage(UserProvider userProvider, BuildContext context) async {
    if (_isFree) {
      await driverRepository.setDriverStatus(currentfirebaseUser.uid, false);
      // detener stream position
//      _stopPositionStream();
      // _stopJobStream();
      await stopStreamSubscription();
      _firestore.collection('availableDrivers').doc(currentfirebaseUser.uid).delete();
      // eliminar al conductor de la colección
    }
    await userProvider.signOut();
  }

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);

    // Obtén una instancia del BotDriverProvider
    final botDriverProvider = Provider.of<BotDriverProvider>(context);
    // Aquí puedes usar el botDriverConfig obtenido del provider
    botDriverProvider.fetchBotDriverConfig(currentfirebaseUser.uid);
    BotDriverConfig? config = botDriverProvider.botDriverConfig;
    bool isConfigBot = botDriverProvider.hasBotDriverConfig;

    List<Widget> solicitudes = []; // Mover la creación de la lista aquí

    return StreamBuilder<Driver?>(
      stream: driverRepository.driver,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final driver = snapshot.data!;
          if (driver.isBlocked) {
            // Mostrar un mensaje al usuario y error sesión
            return const Scaffold(
              body: Center(
                child: Text('Tu cuenta ha sido bloqueada.'),
              ),
            );
          } else {
            //Continuar con la pantalla normal
            return Scaffold(
              appBar: MyCustomAppBar(
                height: kToolbarHeight * 2,
                appBar: AppBar(
                  backgroundColor: primary,
                ),
                bottomWidget: Container(
                    padding: const EdgeInsets.all(20),
                    decoration: const BoxDecoration(
                      color: Colors.transparent,
                    ),
                    height: 50,
                    //(MediaQuery.of(context).size.height / 100) * 23,
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            LockableSwitcher(
                              isLocked: isSwitchLocked,
                              child: Switcher(
                                value: _isFree,
                                size: SwitcherSize.large,
                                switcherButtonRadius: 50,
                                enabledSwitcherButtonRotate: true,
                                iconOff: Icons.lock,
                                iconOn: Icons.lock_open,
                                colorOff: Colors.blueGrey.withOpacity(0.3),
                                colorOn: secundary,
                                onChanged: (bool state) async {
                                  if (!isSwitchLocked) {
                                    if (state) {
                                      isSwitchLocked = true;
                                      await setAvailableDriver();
                                      isSwitchLocked = false;
                                    } else {
                                      await driverRepository.setDriverStatus(currentfirebaseUser.uid, false);
                                      // detener stream position
                                      // _stopJobStream();
                                      await _firestore.collection('availableDrivers').doc(currentfirebaseUser.uid).delete();
                                      // eliminar al conductor de la colección
                                      print('isInitializedRideSubscription: ' + isInitializedRideSubscription.toString());
                                      if (isInitializedRideSubscription) {
                                        _ridesSubscription.cancel(); // detener el stream de solicitudes de taxi
                                      }
                                      _stopPositionStream();
                                      _isFree = false;
                                      if (!_isFree) {
                                        setState(() {
                                          carrerasDisponibles = [];
                                        });
                                      }
                                    }
                                  } else {
                                    print("el interruptor está bloqueado");
                                    Fluttertoast.showToast(
                                      msg: 'El interruptor está bloqueado',
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.white,
                                      fontSize: 16.0,
                                    );
                                  }
                                },
                              ),
                            ),
                            Expanded(
                                //width: (MediaQuery.of(context).size.width / 100) * 35,
                                child: Padding(
                              padding: EdgeInsets.fromLTRB(5, 8, 5, 8),
                              child: Container(
                                  //color: Colors.white,
                                  child: Padding(
                                padding: EdgeInsets.all(5),
                                child: isConfigBot && hasPaidBot
                                    ? Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          ClipOval(
                                            child: Material(
                                              color: isBotActive ? Colors.blue : Colors.red,
                                              child: InkWell(
                                                splashColor: Colors.green,
                                                onTap: () async {
                                                  if (_isFree) {
                                                    setState(() {
                                                      isBotActive = !isBotActive;
                                                    });

                                                    String message = isBotActive ? "Bot activado" : "Bot desactivado";
                                                    Fluttertoast.showToast(
                                                      msg: message,
                                                      toastLength: Toast.LENGTH_SHORT,
                                                      gravity: ToastGravity.BOTTOM,
                                                      backgroundColor: Colors.grey,
                                                      textColor: Colors.white,
                                                      fontSize: 16.0,
                                                    );

                                                    if (isBotActive) {
                                                      //Position position = await _getLocation();
                                                      startBot(currentPosition);
                                                    }
                                                  } else {
                                                    print("active las carreras");
                                                    Fluttertoast.showToast(
                                                      msg: 'Tiene que estar en linea para activar el bot',
                                                      toastLength: Toast.LENGTH_SHORT,
                                                      gravity: ToastGravity.BOTTOM,
                                                      backgroundColor: Colors.grey,
                                                      textColor: Colors.white,
                                                      fontSize: 16.0,
                                                    );
                                                  }

                                                  // Tu función onTap aquí
                                                },
                                                child: Container(
                                                  width: 50,
                                                  height: 50,
                                                  child: const Center(
                                                    child: Text(
                                                      "A",
                                                      style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w800),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    : Row(),
                              )),
                            )),
                          ],
                        ))),
                //),
              ),
              drawer: userProvider.userModel != null
                  ? MenuWidget(
                      modelUser: userProvider.userModel!,
                      onSignOutCallback: () => _signOutAndNavigateToLoginPage(userProvider, context),
                    )
                  : CircularProgressIndicator(),
              body: carrerasDisponibles.isEmpty
                  ? Center(child: Text('No hay carreras disponibles'))
                  : ListView.builder(
                      itemCount: carrerasDisponibles.length,
                      itemBuilder: (BuildContext context, int index) {
                        return RequestItem(
                          listaPasajero: carrerasDisponibles[index],
                          hash: carrerasDisponibles[index].uid,
                          // Assuming id is a property in RideRequest
                          driver: currentDriver!,
                          currrentPosition: currentPosition,
                          driverConfig: config,
                          stopRidesSubscription: stopStreamSubscription,
                          resetRidesCallback: resetRides,
                        );
                      },
                    ),
            );
          }
        } else {
          // Si no hay datos, mostrar pantalla de carga
          //authRepository.signOut();
          return Scaffold(
            body: Center(
              child: Text('no hay datos para mostrar'),
            ),
          );
        }
      },
    );
  }

  Future<void> resetRides() async{
    //isInitializedPositionStreamSubscription = false;
    //stopStreamSubscription();
    driverRepository.setDriverStatus(currentfirebaseUser.uid, true);
    Future.delayed(Duration(seconds: 3), () {
      setState(() {
        _isFree = true;
      });

    });
    if(!isInitializedPositionStreamSubscription){
      await setAvailableDriver();
    }
  }

// Función para calcular la distancia y el tiempo estimado en minutos
  Map<String, String> calculateDistanceAndTime(Position currentPosition, Map<String, dynamic> pickupLocation) {
    double pickupLat = double.parse(pickupLocation['latitude']);
    double pickupLng = double.parse(pickupLocation['longitude']);

    double distanceInMeters = Geolocator.distanceBetween(
      currentPosition.latitude,
      currentPosition.longitude,
      pickupLat,
      pickupLng,
    );

    double distanceInKm = distanceInMeters / 1000;
    String distanciaConductorPasajero = '${distanceInKm.toStringAsFixed(2)} km';

    double velocidadPromedioKmPorHora = 30.0; // Velocidad promedio en km/h
    double tiempoEstimadoEnHoras = distanceInKm / velocidadPromedioKmPorHora;
    int tiempoEstimadoEnMinutos = (tiempoEstimadoEnHoras * 60).toInt();
    String tiempoConductorPasajero = '$tiempoEstimadoEnMinutos minutos';

    return {
      'distancia_conductor_pasajero': distanciaConductorPasajero,
      'tiempo_conductor_pasajero': tiempoConductorPasajero,
    };
  }

  Future<void> stopStreamSubscription() async {
    print('deteniendo');
    if (isInitializedRideSubscription) {
      print('cancelar ridesSuscripcion');

      _ridesSubscription.cancel();
    }

    _stopPositionStream();

  }

  Future<double> calculateDistanceToPickup(Position currentPosition, Map<String, dynamic> pickupLocation) async {
    double pickupLatitude = double.parse(pickupLocation['latitude']);
    double pickupLongitude = double.parse(pickupLocation['longitude']);

    double distanceInMeters = await Geolocator.distanceBetween(
      currentPosition.latitude,
      currentPosition.longitude,
      pickupLatitude,
      pickupLongitude,
    );

    // Convertir la distancia a kilómetros
    double distanceInKm = distanceInMeters / 1000;
    return distanceInKm;
  }

  Future<void> setAvailableDriver() async {

    await driverRepository.setDriverStatus(currentfirebaseUser.uid, true);
    setState(() {
      _isFree = true;
    });
    _initPositionStream('availableDrivers');

    stream = geo.collection(collectionRef: _firestore.collection('rideRequests')).within(
          center: GeoFirePoint(currentPosition.latitude, currentPosition.longitude),
          radius: radius,
          field: 'position',
          strictMode: true,
        );
    print('iniciando escucha carreras');
    _ridesSubscription = stream.listen(_handleRideRequests);

    setState(() {
      isInitializedRideSubscription = true;
    });
  }

  Future<void> _handleRideRequests(List<DocumentSnapshot> rides) async {
    print('_handleRideRequests');
    String rideIdUpdated = ''; // Variable para almacenar el ID de la solicitud actualizada
    String rideIdRemoved = ''; // Variable para almacenar el ID de la solicitud eliminada
    String rideIdNew = ''; // Variable para almacenar el ID de la solicitud eliminada

    Position position = await _getLocation();
    // Filtrar por categoría
    rides.removeWhere((element) => element['rideInfoMap']['tipo_vehiculo'] != currentDriver.type);
    List<String> existingdRideIds = carrerasDisponibles.map((ride) => ride.uid).toList();
    List<String> updatedRideIds = rides.map((ride) => ride.id).toList();
    bool didRemoveRides = false; // Variable para rastrear si se eliminaron solicitudes
    bool hasNewRides = false; // Bandera para indicar si hay nuevas solicitudes

    for (DocumentSnapshot ride in rides) {
      String newRideId = ride.id;
      Map<String, dynamic> updatedRideData = ride.data() as Map<String, dynamic>;

      // Verificar si la solicitud ya existe en la lista
      int existingIndex = carrerasDisponibles.indexWhere((existingRide) => existingRide.uid == newRideId);
      if (existingIndex != -1) {
        // Solicitud existente, actualiza si es necesario
        RideRequest existingRide = carrerasDisponibles[existingIndex];
        RideRequest updatedRide = RideRequest.fromMap(updatedRideData['rideInfoMap']);
        updatedRide.conductoresDistanciaTiempo = existingRide.conductoresDistanciaTiempo;
        updatedRide.uid = newRideId;
        if (!existingRide.isEquivalentTo(updatedRide)) {
          // Actualizar el objeto existente en lugar de reemplazarlo
          existingRide.updateFrom(updatedRide);
          print('Solicitud actualizada con ID: $newRideId');
          rideIdUpdated = newRideId;
        }
      } else {
        // Solicitud nueva, agrégala a la lista
        RideRequest rideRequest = RideRequest.fromMap(updatedRideData['rideInfoMap']);
        int conductorCode = currentDriver.code;

        // Calcular distancia y tiempo
        Map<String, String> distanceAndTime = calculateDistanceAndTime(position, rideRequest.pickUpLocMap);

        // Agregar distancia y tiempo a la solicitud de viaje
        rideRequest.conductoresDistanciaTiempo = {
          conductorCode.toString(): {
            'conductor_code': conductorCode,
            'distancia_conductor_pasajero': distanceAndTime['distancia_conductor_pasajero'],
            'tiempo_conductor_pasajero': distanceAndTime['tiempo_conductor_pasajero'],
          }
        };
        rideRequest.uid = newRideId;
        carrerasDisponibles.add(rideRequest);
        hasNewRides = true; // Marcar que se encontró al menos una nueva solicitud
        print('Nueva solicitud con ID: $newRideId');
      }
    }

    carrerasDisponibles.removeWhere((existingRide) => !updatedRideIds.contains(existingRide.uid));
    carrerasDisponibles.sort(compareByDistance);
    _ridesObserver.notifyListeners();

    for (RideRequest existingRide in carrerasDisponibles.toList()) {
      // Agrega lógica para detectar la eliminación de una solicitud
      if (!updatedRideIds.contains(existingRide.uid)) {
        // Almacena el ID de la solicitud eliminada y actualiza la bandera
        rideIdRemoved = existingRide.uid;
        didRemoveRides = true;
        print('didremoveridesTrue');
      }
    }
    // if(!carrerasDisponibles.contains(rideIdNew)){
    //   didRemoveRides = true;
    // }
    if (carrerasDisponibles.length == updatedRideIds.length) {
      didRemoveRides = false;
    }

    // ... tu código existente para agregar y ordenar las solicitudes ...

    // Solo inicia el bot si hay nuevas o actualizadas solicitudes y no se eliminaron solicitudes
    // Verifica si hubo cambios en las solicitudes y si es necesario iniciar el bot
    print('a punto de iniciar');
    print(rideIdUpdated);
    print(hasNewRides);
    print(updatedRideIds.contains(rideIdUpdated));
    print(updatedRideIds);
    print("carreras tomadas");
    print(carrerasTomadas);
    print(carrerasDisponibles);
    bool faltanCarrerasPorTomar = false;
    for(String updatedRideId in updatedRideIds) {
      if (!carrerasTomadas.contains(updatedRideId)) {
        faltanCarrerasPorTomar = true;
        hasNewRides = true;
        break;
      }
    }
    print(faltanCarrerasPorTomar);
    // Solo inicia el bot si hay nuevas o actualizadas solicitudes y no se eliminaron solicitudes
    if (isBotActive && (hasNewRides || updatedRideIds.contains(rideIdUpdated)) && faltanCarrerasPorTomar && !botBusy) {
      if (updatedRideIds.contains(rideIdUpdated)) {
        offeredRideIds.remove(rideIdUpdated);
      }
      botBusy = true;
      await startBot(currentPosition);
      botBusy = false;
    }
  }

  Future<void> startBot(Position position) async {
    print('ya inicio bot');
    //_ridesSubscription.cancel();
    BotDriverService botDriverService = BotDriverService();
    BotDriverConfig? config = await botDriverService.getBotDriverConfig(currentfirebaseUser.uid);

    if (isBotActive) {
      List<String> updatedRideIds = carrerasDisponibles.map((ride) => ride.uid).toList();

      // Filtrar las carreras que ya no están presentes en la lista de carreras disponibles
      List<String> removedRideIds = offeredRideIds.where((rideId) => !updatedRideIds.contains(rideId)).toList();

      if (!tookInitialRides) {
        // Tomar todas las carreras disponibles inicialmente
        List<RideRequest> ridesCopy = List.from(carrerasDisponibles); // Crear una copia de la lista original

        for (RideRequest ride in ridesCopy) {
          String hash = ride.uid;
          bool? tookRide = await makeBotConfig(position, ride, hash, config);
          if (tookRide != null && tookRide) {
            carrerasTomadas.add(hash);
            break;
          }

          print(offeredRideIds);
        }
        resetRides();
        tookInitialRides = true;
      } else {
        print('solo nuevo y actualizados');
        // Solo procesar carreras nuevas o actualizadas después de la activación inicial
        List<RideRequest> newOrUpdatedRides = carrerasDisponibles.where((ride) => !offeredRideIds.contains(ride.uid)).toList();
        List<RideRequest> ridesCopy = List.from(carrerasDisponibles); // Crear una copia de la lista original
        for (RideRequest ride in ridesCopy) {
          if (!offeredRideIds.contains(ride.uid)) {
            print(offeredRideIds);
            String hash = ride.uid;
            bool? tookRide = await makeBotConfig(position, ride, hash, config);
            if (tookRide != null && tookRide) {
              //resetRides();
              carrerasTomadas.add(hash);
              break;
            }
            print(offeredRideIds);
          }
        }
        resetRides();
      }

      // Eliminar las carreras que han sido eliminadas
      //offeredRideIds.removeWhere((rideId) => removedRideIds.contains(rideId));
    }
  }

  Future<bool?> makeBotConfig(Position position, RideRequest rideRequest, String hash, BotDriverConfig? botDriverConfig) async {
    print('makeBot');

    if (botDriverConfig != null) {
      BotDriverConfigA? configuracionA = botDriverConfig.configuracionA;

      if (configuracionA != null) {
        double tarifa_por_km = configuracionA.tarifa_por_km;
        double tarifa_maxima = configuracionA.tarifa_maxima;
        double distancia_maxima_conductor_origen = configuracionA.distancia_maxima_conductor_origen;
        double distancia_maxima_origen_destino = configuracionA.distancia_maxima_origen_destino;

        ///seteo variables del bot si son nulas
        if (configuracionA?.tarifa_maxima == 0.0) {
          tarifa_maxima = 1000000;
        }
        if (configuracionA?.distancia_maxima_conductor_origen == 0.0) {
          distancia_maxima_conductor_origen = 1000000;
        }
        if (configuracionA?.distancia_maxima_origen_destino == 0.0) {
          distancia_maxima_origen_destino = 1000000;
        }

        ///distancia conductor-origen
        double distanciaConductorOrigen = await calculateDistanceToPickup(position, rideRequest.pickUpLocMap);
        print(distancia_maxima_conductor_origen);
        print(distanciaConductorOrigen);

        ///distancia origen-destino
        String distanceString = rideRequest.distanceText;
        String numericString = distanceString.replaceAll(RegExp(r'[^0-9.]'), ''); // Elimina caracteres no numéricos
        double distanciaOrigenDestino = double.tryParse(numericString) ?? 0.0; // Convierte a double, si no se puede, asigna 0.0
        print(distancia_maxima_origen_destino);
        print(distanciaOrigenDestino);

        ///tarifa maxima
        double tarifaMaximaRideRequest = double.parse(rideRequest.tarifaOfertada);

        ///tarifa minima
        double tarifaMinima = configuracionA.tarifa_minima;
        double tarifaMinimaRideRequest = double.parse(rideRequest.tarifaSugerida);
        print('tarifa maxima: '+ tarifa_maxima.toString());
        print('tarifa maxima ride: '+ tarifaMaximaRideRequest.toString());
        if(rideRequest.tipoCarrera == 'taxi_a_la_ubicacion'){
          print('es taxi a la ubicacion');
          bool algunaNotaEnDeseo = false;


          if (rideRequest.deseo != '') {
            String notasString = configuracionA.filtrarNotas;
            //List<String> notas = notasString.split(',');
            List<String> notas = notasString.split(',').where((note) => note.trim().isNotEmpty).toList();
            for (String nota in notas) {
              String notaSinEspacios = nota.replaceAll(' ', ''); // Quita los espacios de la cadena
              if (rideRequest.deseo.contains(notaSinEspacios)) {
                algunaNotaEnDeseo = true;
                break; // Terminamos el ciclo ya que hemos encontrado una nota en el deseo
              }
            }
          }
          if (!algunaNotaEnDeseo && distanciaConductorOrigen <= distancia_maxima_conductor_origen) {
            // Hacer algo si al menos una nota está en el deseo
            return await processAutomaticAcceptance(rideRequest, hash, currentPosition);
          }

        }else if(distanciaConductorOrigen <= distancia_maxima_conductor_origen &&
            distanciaOrigenDestino <= distancia_maxima_origen_destino &&
            tarifa_maxima >= tarifaMaximaRideRequest &&
            tarifaMinima <= tarifaMinimaRideRequest) {
          print('tarifa maxima: '+ tarifa_maxima.toString());
          print('tarifa maxima ride: '+ tarifaMaximaRideRequest.toString());

          ///verificar si esta en lista de radios.
          List<ZonaTrabajo> zonasTrabajos = configuracionA.listaZonaTrabajo;
          // Verificar si el origen y el destino están dentro de las zonas de trabajo
          bool flagWorkArea = false;
          if(zonasTrabajos.length == 0){
            flagWorkArea = true;
          }
          bool flagWorkAreaPickUp = false; // Bandera para el origen (pick up)
          bool flagWorkAreaDropOff = false; // Bandera para el destino (drop off)

          for (var element in zonasTrabajos) {
            LatLng mapPositionA = LatLng(double.parse(rideRequest.pickUpLocMap['latitude']!), double.parse(rideRequest.pickUpLocMap['longitude']!));
            LatLng mapPositionB = LatLng(double.parse(rideRequest.dropOffLocMap['latitude']!), double.parse(rideRequest.dropOffLocMap['longitude']!));
            LatLng radiusPosition = LatLng(element.center.latitude, element.center.longitude);
            double radius = element.radius;
            bool banderaZonaTrabajoA = isLocationWithinRange(mapPositionA, radiusPosition, radius);
            bool banderaZonaTrabajoB = isLocationWithinRange(mapPositionB, radiusPosition, radius);

            if (banderaZonaTrabajoA) {
              flagWorkAreaPickUp = true; // Establece la bandera del origen
            }

            if (banderaZonaTrabajoB) {
              flagWorkAreaDropOff = true; // Establece la bandera del destino
            }

            // Otras acciones si es necesario...
          }




          ///tarifa por kilometro
          print('tarifa por kilometro rideRequest');
          double tarifaPorKilometroRideRequest = double.parse(rideRequest.tarifaSugerida) / distanciaOrigenDestino;
          print('flagWorkArea: '+ flagWorkArea.toString());
          if (flagWorkAreaPickUp && flagWorkAreaDropOff) {
            bool algunaNotaEnDeseo = false;
            if (rideRequest.deseo != '') {
              String notasString = configuracionA.filtrarNotas;
              //List<String> notas = notasString.split(',');
              List<String> notas = notasString.split(',').where((note) => note.trim().isNotEmpty).toList();
              for (String nota in notas) {
                String notaSinEspacios = nota.replaceAll(' ', ''); // Quita los espacios de la cadena
                if (rideRequest.deseo.contains(notaSinEspacios)) {
                  algunaNotaEnDeseo = true;
                  break; // Terminamos el ciclo ya que hemos encontrado una nota en el deseo
                }
              }
            }
            if (!algunaNotaEnDeseo) {
              // Hacer algo si al menos una nota está en el deseo
              if (tarifaPorKilometroRideRequest <= tarifa_por_km) {
                if (!offeredRideIds.contains(hash)) {
                  // Realizar oferta
                  print('realizar oferta');
                  // Desactivar el bot después de procesar una carrera
                  isBotBusy = true;
                  //_ridesSubscription.cancel();
                  var res = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetalleCarreraScreen(
                        rideDetails: rideRequest,
                        hash: hash,
                        driver: currentDriver,
                        makeOffer: true,
                        currrentPosition: currentPosition,
                        config: configBot,
                        stopRidesSubscription: stopStreamSubscription,
                      ),
                    ),
                  );
                  print('carrera finalizada');
                  if (res == 'oferta_rechazada') {
                    print('oferta_rechazada');
                    await Future.delayed(Duration(seconds: 10)); // Esperar 10 segundos antes de continuar
                    offeredRideIds.add(hash);
                    return false;
                  }
                  return true; // Indicar que se ha tomado una carrera
                } else {
                  print('Carrera ya ofertada y rechazada anteriormente.');
                }
              } else {
                //_ridesSubscription.cancel();
                // Aceptar directamente
                print('aceptar directamente');
                print('carrera aceptada automaticamente distancia maxima conductor origen');
                isBotBusy = true;
                stopStreamSubscription();
                var res = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetalleCarreraScreen(
                      rideDetails: rideRequest,
                      hash: hash,
                      driver: currentDriver,
                      currrentPosition: currentPosition,
                      makeOffer: false,
                      config: configBot,
                      stopRidesSubscription: stopStreamSubscription,
                    ),
                  ),
                );
                 //await setAvailableDriver();
                await Future.delayed(Duration(seconds: 5)); // Esperar 10 segundos antes de continuar

                return true; // Indicar que se ha tomado una carrera
              }
            }


          }

          return false; // Indicar que no se ha tomado una carrera

        }

      }
    }
  }

  // Future<bool?> makeBotConfig(Position position, RideRequest rideRequest, String hash, BotDriverConfig? botDriverConfig) async {
  //   print('makeBot');
  //
  //   if (botDriverConfig == null) {
  //     return null;
  //   }
  //
  //   final configuracionA = botDriverConfig.configuracionA;
  //   if (configuracionA == null) {
  //     return null;
  //   }
  //
  //   double tarifa_por_km = configuracionA.tarifa_por_km;
  //   double tarifa_maxima = configuracionA.tarifa_maxima;
  //   double distancia_maxima_conductor_origen = configuracionA.distancia_maxima_conductor_origen;
  //   double distancia_maxima_origen_destino = configuracionA.distancia_maxima_origen_destino;
  //
  //   tarifa_maxima ??= 1000000;
  //   distancia_maxima_conductor_origen ??= 1000000;
  //   distancia_maxima_origen_destino ??= 1000000;
  //
  //   double distanciaConductorOrigen = await calculateDistanceToPickup(position, rideRequest.pickUpLocMap);
  //   double distanciaOrigenDestino = parseDistanceFromString(rideRequest.distanceText);
  //   double tarifaMaximaRideRequest = double.parse(rideRequest.tarifaOfertada);
  //   double tarifaMinimaRideRequest = double.parse(rideRequest.tarifaSugerida);
  //
  //   ///tarifa minima
  //   double tarifaMinima = configuracionA.tarifa_minima;
  //   if (distanciaConductorOrigen <= distancia_maxima_conductor_origen &&
  //       distanciaOrigenDestino <= distancia_maxima_origen_destino &&
  //       tarifa_maxima >= tarifaMaximaRideRequest &&
  //       tarifaMinima <= tarifaMinimaRideRequest) {
  //     bool flagWorkArea = isInWorkArea(rideRequest, configuracionA.listaZonaTrabajo);
  //
  //     double tarifaPorKilometroRideRequest = tarifaMaximaRideRequest / distanciaOrigenDestino;
  //
  //     if (flagWorkArea) {
  //       if (tarifaPorKilometroRideRequest <= tarifa_por_km) {
  //         return await processOffer(rideRequest, hash, currentPosition);
  //       } else {
  //         return await processAutomaticAcceptance(rideRequest, hash, currentPosition);
  //       }
  //     }
  //
  //     return false;
  //   }else if(rideRequest.tipoCarrera == 'taxi_a_la_ubicacion'){
  //     print('es taxi a la ubicacion');
  //     bool algunaNotaEnDeseo = false;
  //
  //     if (rideRequest.deseo != '') {
  //       String notasString = configuracionA.filtrarNotas;
  //       List<String> notas = notasString.split(',');
  //
  //       for (String nota in notas) {
  //         if (rideRequest.deseo.contains(nota)) {
  //           algunaNotaEnDeseo = true;
  //           break; // Terminamos el ciclo ya que hemos encontrado una nota en el deseo
  //         }
  //       }
  //     }
  //
  //     if (!algunaNotaEnDeseo) {
  //       // Hacer algo si al menos una nota está en el deseo
  //       return await processAutomaticAcceptance(rideRequest, hash, currentPosition);
  //     }
  //   }
  //
  //
  //   return null;
  // }

  bool isInWorkArea(RideRequest rideRequest, List<ZonaTrabajo> zonasTrabajos) {
    for (var element in zonasTrabajos) {
      LatLng mapPositionA = LatLng(double.parse(rideRequest.pickUpLocMap['latitude']!), double.parse(rideRequest.pickUpLocMap['longitude']!));
      LatLng mapPositionB = LatLng(double.parse(rideRequest.dropOffLocMap['latitude']!), double.parse(rideRequest.dropOffLocMap['longitude']!));
      LatLng radiusPosition = LatLng(element.center.latitude, element.center.longitude);
      double radius = element.radius;
      bool banderaZonaTrabajoA = isLocationWithinRange(mapPositionA, radiusPosition, radius);
      bool banderaZonaTrabajoB = isLocationWithinRange(mapPositionB, radiusPosition, radius);
      if (banderaZonaTrabajoA && banderaZonaTrabajoB) {
        return true;
      }
    }
    return false;
  }

  double parseDistanceFromString(String distanceString) {
    String numericString = distanceString.replaceAll(RegExp(r'[^0-9.]'), '');
    return double.tryParse(numericString) ?? 0.0;
  }

  Future<bool> processOffer(RideRequest rideRequest, String hash, Position currentPosition) async {
    if (!offeredRideIds.contains(hash)) {
      // Realizar oferta
      print('realizar oferta');
      isBotBusy = true;
      var res = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetalleCarreraScreen(
            rideDetails: rideRequest,
            hash: hash,
            driver: currentDriver,
            makeOffer: true,
            currrentPosition: currentPosition,
            stopRidesSubscription: stopStreamSubscription,
          ),
        ),
      );
      print('carrera finalizada');
      if (res == 'oferta_rechazada') {
        print('oferta_rechazada');
        await Future.delayed(Duration(seconds: 10));
        offeredRideIds.add(hash);
        return false;
      }
      return true;
    } else {
      print('Carrera ya ofertada y rechazada anteriormente.');
    }
    return false;
  }

  Future<bool> processAutomaticAcceptance(RideRequest rideRequest, String hash, Position currentPosition) async {
    print('aceptar directamente');
    print('carrera aceptada automaticamente distancia maxima conductor origen');
    isBotBusy = true;
    var res = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetalleCarreraScreen(
          rideDetails: rideRequest,
          hash: hash,
          driver: currentDriver,
          currrentPosition: currentPosition,
          makeOffer: false,
          config: configBot,
          stopRidesSubscription: stopStreamSubscription,
        ),
      ),
    );
    await Future.delayed(Duration(seconds: 10));
    return true;
  }

  int compareByDistance(RideRequest a, RideRequest b) {
    if(a.tipoCarrera=='taxi_a_la_ubicacion'){
      a.distanceText='0 km';
    }
    if(b.tipoCarrera=='taxi_a_la_ubicacion'){
      b.distanceText='0 km';
    }

    // Obtener la distancia como número (puedes usar la lógica que tengas para calcular la distancia)
    double distanceA = double.parse(a.distanceText.split(" ")[0]);
    double distanceB = double.parse(b.distanceText.split(" ")[0]);

    return distanceA.compareTo(distanceB);
  }

  double calculateDistance(LatLng from, LatLng to) {
    double distance = Geolocator.distanceBetween(
      from.latitude,
      from.longitude,
      to.latitude,
      to.longitude,
    );
    return distance;
  }

  // Cuando se rechaza una oferta de carrera
  void rejectOffer(String rideId) {
    if (!offeredRideIds.contains(rideId)) {
      offeredRideIds.add(rideId);
    }
    // Resto del código para rechazar la oferta
  }

  bool isLocationWithinRange(LatLng mapPosition, LatLng radiusPosition, double radius) {
    try {
      String message;
      Color snackColor;
      bool isWithinRange = false;

      // Calcula la distancia entre las dos ubicaciones utilizando la fórmula de la distancia de gran círculo
      double gcd = calculateDistance(LatLng(mapPosition.latitude, mapPosition.longitude), LatLng(radiusPosition.latitude, radiusPosition.longitude));

      // Compara la distancia calculada con el radio proporcionado
      if (radius >= gcd) {
        message = 'La localización se encuentra dentro del rango a ${gcd} Mts';
        snackColor = Colors.green;
        isWithinRange = true;
      } else {
        message = 'La localización se encuentra fuera del rango a ${gcd} Mts';
        snackColor = Colors.red;
        isWithinRange = false;
      }

      // Crea y muestra el SnackBar con el mensaje apropiado
      final snackBar = SnackBar(
        content: Text(message),
        duration: Duration(seconds: 3),
        backgroundColor: snackColor,
      );

      // Crea el marcador con la información adecuada
      final marker = Marker(
        markerId: MarkerId(mapPosition.toString()),
        position: mapPosition,
        infoWindow: InfoWindow(
          title: '${gcd} Mts del radio',
        ),
        icon: snackColor == Colors.green ? BitmapDescriptor.defaultMarkerWithHue(130.0) : BitmapDescriptor.defaultMarker,
      );

      // Retorna si la localización está dentro del rango
      return isWithinRange;
    } catch (error) {
      print("Error al verificar si está en el radio: $error");
      return false;
    }
  }

// Definir una función de comparación para ordenar por distancia
//   int compareByDistance(DocumentSnapshot a, DocumentSnapshot b) {
//     int conductorCode = currentDriver.code;
//
//     final distanciaA = getDistanceForConductor(a, conductorCode);
//     final distanciaB = getDistanceForConductor(b, conductorCode);
//
//     print('Comparando: distanciaA = $distanciaA, distanciaB = $distanciaB');
//
//     if (distanciaA != null && distanciaB != null) {
//       return distanciaA.compareTo(distanciaB); // Ordenar de menor a mayor
//     } else {
//       return 0;
//     }
//   }

// Obtener la distancia para un conductor específico en el documento
  double? getDistanceForConductor(DocumentSnapshot ride, int conductorCode) {
    final rideData = ride.data() as Map<String, dynamic>?;

    if (rideData != null) {
      final rideInfoMap = rideData['rideInfoMap'] as Map<String, dynamic>?;

      if (rideInfoMap != null) {
        final conductoresDistanciaTiempo = rideInfoMap['conductores_distancia_tiempo'];

        if (conductoresDistanciaTiempo != null && conductoresDistanciaTiempo is Map) {
          final conductorData = conductoresDistanciaTiempo[conductorCode.toString()];

          if (conductorData != null) {
            final distanciaConductor = conductorData['distancia_conductor_pasajero'];

            if (distanciaConductor is String) {
              final distanciaString = distanciaConductor.split(' ')[0]; // Obtener la parte numérica del string
              return double.tryParse(distanciaString);
            }
          }
        }
      }
    }

    return null;
  }

  Map<String, dynamic>? getCurrentConductorInfo(List<DocumentSnapshot> rides) {
    for (DocumentSnapshot ride in rides) {
      Map<String, dynamic> rideData = ride.data() as Map<String, dynamic>;
      Map<String, dynamic> rideInfoMap = rideData['rideInfoMap'];

      List<dynamic> conductoresDistanciaTiempo = rideInfoMap['conductores_distancia_tiempo'] ?? [];

      var currentConductor = conductoresDistanciaTiempo.firstWhere((conductor) => conductor['conductor_code'] == currentDriver.code, orElse: () => null);

      if (currentConductor != null) {
        return currentConductor;
      }
    }

    return null;
  }
}
