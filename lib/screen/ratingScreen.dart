import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating_null_safety/smooth_star_rating_null_safety.dart';


import '../helpers/style.dart';
import '../variables_constantes.dart';

class RatingScreen extends StatefulWidget {
  static const String idScreen = "ratingScreen";

  final String? driverId;

  const RatingScreen({Key? key, this.driverId}) : super(key: key);

  @override
  _RatingScreenState createState() => _RatingScreenState();
}

class _RatingScreenState extends State<RatingScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false, // Esta línea bloquea la acción de retroceder
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        body: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          backgroundColor: Colors.transparent,
          child: Container(
            margin: EdgeInsets.all(5.0),
            width: double.infinity,
            decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 22.0),
                Text("Califica al pasajero", style: TextStyle(fontSize: 20.0, fontFamily: "Brand Bold", color: Colors.black54)),
                SizedBox(height: 22.0),
                Divider(
                  height: 2.0,
                  thickness: 2.0,
                ),
                SizedBox(height: 16.0),
                SmoothStarRating(
                  rating: starCounter,
                  color: primary,
                  allowHalfRating: false,
                  starCount: 5,
                  size: 45,
                  onRatingChanged: (value) {
                    starCounter = value;
                    if (starCounter == 1) {
                      setState(() {
                        title = "Muy malo";
                      });
                    }
                    if (starCounter == 2) {
                      setState(() {
                        title = "Malo";
                      });
                    }
                    if (starCounter == 3) {
                      setState(() {
                        title = "Bueno";
                      });
                    }
                    if (starCounter == 4) {
                      setState(() {
                        title = "Muy bueno";
                      });
                    }
                    if (starCounter == 5) {
                      setState(() {
                        title = "Excelente";
                      });
                    }
                  },
                ),
                SizedBox(height: 14.0),
                Text(title, style: TextStyle(fontSize: 55.0, fontFamily: "Signatra", color: black)),
                SizedBox(height: 16.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: TextButton(
                    onPressed: () async {
                      saveRatings(starCounter, widget.driverId);
                      Navigator.pop(context, "close");
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: secundary,
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "Enviar",
                            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30.0),
              ],
            ),
          ),
        ),
      ),
    );
  }


  Future<void> saveRatings(double rating, String? uid) async {
    final userRef = FirebaseFirestore.instance.collection('users').doc(uid);

    userRef.get().then((docSnapshot) {
      if (docSnapshot.exists) {
        //    double oldRatings = double.parse(snap.value.toString());
        //  double addRatings = oldRatings + starCounter;
//        double averageRatings = addRatings/2;
        //      driverRatingRef.set(averageRatings.toString());

        String earningsStr = docSnapshot.data()?['rating'] ?? '0';
        double oldEarnings = double.parse(earningsStr);
        double addRatings = rating + oldEarnings;
        double averageRatings = addRatings / 2;

        userRef.update({'rating': averageRatings.toStringAsFixed(2)});
      } else {
        //double totalEarnings = fareAmount.toDouble();
        userRef.set({'rating': rating.toStringAsFixed(2)});
      }
    });
  }
}
