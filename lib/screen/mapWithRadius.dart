import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math' as math;

class MapWithRadius extends StatefulWidget {
  final Position position;
  final List<Circle> initialCircles;
  MapWithRadius({
    Key? key,
    required this.position, required this.initialCircles,
  }) : super(key: key);

  @override
  _MapWithRadiusState createState() => _MapWithRadiusState();
}

class _MapWithRadiusState extends State<MapWithRadius> {
  Circle? _selectedCircle;
  late Position position;
  late GoogleMapController _mapController;
  LatLng _center = LatLng(37.7749, -122.4194); // Centro inicial del mapa
  double _radius = 1000.0; // Radio inicial en metros
  Set<Circle> _circles = {}; // Conjunto de círculos en el mapa
  int _circleCount = 1; // Contador de círculos
  double _zoomLevel = 14; // Initialize with a default zoom level

  @override
  @override
  void initState() {
    super.initState();
    position = widget.position;
    _center = LatLng(position.latitude, position.longitude);

    // Load initial circles from widget parameter
    //_circles.addAll(widget.initialCircles);
    print(widget.initialCircles);
    _addCirclesFromZones(widget.initialCircles);
  }
  void _addCirclesFromZones(List<Circle> zonesData) {
    for (var zoneData in zonesData) {
      double latitude = zoneData.center.latitude;
      double longitude = zoneData.center.longitude;
      double radius = zoneData.radius;

      _circles.add(
        Circle(
          circleId: CircleId(latitude.toString() + longitude.toString()),
          center: LatLng(latitude, longitude),
          radius: radius,
          fillColor: Colors.blue.withOpacity(0.2),
          strokeWidth: 2,
          strokeColor: Colors.blue,
        ),
      );
    }
  }
  void _onCircleTapped(Circle circle) {
    setState(() {
      _selectedCircle = circle;
    });
  }


  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }

  // void _onMapTap(LatLng latLng) {
  //   // Check if the tap is within any existing circle
  //   Circle? tappedCircle;
  //   for (var circle in _circles) {
  //     final distance = math.sqrt(
  //       math.pow(circle.center.latitude - latLng.latitude, 2) +
  //           math.pow(circle.center.longitude - latLng.longitude, 2),
  //     );
  //     if (distance <= circle.radius) {
  //       tappedCircle = circle;
  //       break;
  //     }
  //   }
  //
  //   if (tappedCircle != null) {
  //     _onCircleTapped(tappedCircle);
  //   } else {
  //     setState(() {
  //       _center = latLng;
  //       _selectedCircle = null;
  //       _updateCirclePosition();
  //     });
  //   }
  // }

  void _onMapTap(LatLng latLng) {
    setState(() {
      _center = latLng;
      _updateCirclePosition();
    });
  }


  void _onRadiusChanged(double value) {
    setState(() {
      _radius = value;
      _updateCircleRadius();
    });
  }

  void _removeCircle(CircleId circleId) {
    setState(() {
      _circles.removeWhere((circle) => circle.circleId == circleId);
    });
  }

  void _addCircle() {
    final CircleId circleId = CircleId('circle$_circleCount'); // Numeración del círculo
    final Circle circle = Circle(
      circleId: circleId,
      center: _center,
      radius: _radius,
      fillColor: Colors.blue.withOpacity(0.3),
      strokeColor: Colors.blue,
      strokeWidth: 2,
    );

    setState(() {
      _circles.add(circle);
      _circleCount++;
    });
  }

  void _updateCirclePosition() {
    _circles.removeWhere((circle) => circle.circleId.value == 'current');
    final CircleId circleId = CircleId('current');
    final Circle circle = Circle(
      circleId: circleId,
      center: _center,
      radius: _radius,
      fillColor: Colors.blue.withOpacity(0.3),
      strokeColor: Colors.blue,
      strokeWidth: 2,
    );
    _circles.add(circle);
  }

  void _updateCircleRadius() {
    _circles.removeWhere((circle) => circle.circleId.value == 'current');
    final CircleId circleId = CircleId('current');
    final Circle circle = Circle(
      circleId: circleId,
      center: _center,
      radius: _radius,
      fillColor: Colors.blue.withOpacity(0.3),
      strokeColor: Colors.blue,
      strokeWidth: 2,
    );
    _circles.add(circle);
  }

  @override
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
      Navigator.pop(context, _circles); // Return circles when user presses back
      return true;
    },
    child:  Scaffold(
      appBar: AppBar(title: Text('Area de trabajo')),
      body: Column(
        children: [
          Expanded(
            child: GoogleMap(
              initialCameraPosition: CameraPosition(target: _center, zoom: 14),
              onMapCreated: _onMapCreated,
              circles: _circles,
              onTap: _onMapTap,
              myLocationEnabled: true,

            ),
          ),
          Slider(
            value: _radius,
            min: 100.0,
            max: 5000.0,
            onChanged: _onRadiusChanged,
          ),
          Text(
            'Diámetro: ${((_radius * 2) / 1000).toStringAsFixed(2)} km',
            style: TextStyle(fontSize: 16),
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
            onPressed: _addCircle,
            child: Text('Agregar circulo'),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _circles.length,
              itemBuilder: (context, index) {
                final circle = _circles.elementAt(index);
                final isSelected = circle.circleId.value == _selectedCircle?.circleId.value;

                return ListTile(
                  title: Text('${circle.circleId.value}'),
                  trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () => _removeCircle(circle.circleId),
                  ),
                  tileColor: isSelected ? Colors.blue.withOpacity(0.1) : null,
                  onTap: () => _onCircleTapped(circle),
                );
              },
            ),

          ),
          // Overlay Text widget over each circle
        ],
      ),
    ),
    );
  }
}
