import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/model/botDriverConfig.dart';
import 'package:zmovil_conductor/screen/verMapaScreen.dart';
import '../model/driver.dart';
import '../provider/user.dart';
import '../helpers/style.dart';
import '../widget/addListNote.dart';
import '../widget/labelChecker.dart';
import 'mapWithRadius.dart';

class BotPage extends StatefulWidget {
  final Driver? drive; // Agrega este campo al constructor

  BotPage({this.drive}); // Agrega este constructor
  static const String idScreen = "botPage";

  @override
  _BotPageState createState() => _BotPageState();
}

class _BotPageState extends State<BotPage> {
  final List<Circle> initialCircles = []; // Pass initial circles here if needed
  List<Map<String, dynamic>> circleDataList = [];

  CollectionReference botDriverRef = FirebaseFirestore.instance.collection('botDriver');
  String tipoSuscripcion = "";
  bool _isSelected = false;
  TextEditingController inputTarifaPorKmControllerA = TextEditingController();
  TextEditingController inputTarifaMinimaControllerA = TextEditingController();
  TextEditingController inputTarifaMaximaControllerA = TextEditingController();
  TextEditingController inputDistanciaMaximaConductorOrigenControllerA = TextEditingController();
  TextEditingController inputDistanciaMaximaOrigenDestinoControllerA = TextEditingController();
  TextEditingController inputMensajeWhatsappControllerA = TextEditingController();
  TextEditingController inputZonaTrabajoControllerA = TextEditingController();
  TextEditingController inputFiltrarNotasControllerA = TextEditingController();

  TextEditingController inputDistanciaMaximaConductorOrigenControllerB = TextEditingController();
  TextEditingController inputFiltrarNotasControllerB = TextEditingController();

  TextEditingController inputZonaDescanzoControllerC = TextEditingController();
  late Position position;
  List<String> selectedNotes = []; // Lista para almacenar las notas seleccionadas

  @override
  void initState() {
    super.initState();
    init();
    UserProvider userProvider1 = Provider.of<UserProvider>(context,listen: false);
    if (userProvider1.botDriverData != null) {
      setDatos(userProvider1.botDriverData!);
    }

    print('init state circleDataList: ' + circleDataList.length.toString());
  }

  Future<Position> _getLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
    return position;
  }

  Future<void> init() async {
    position = await _getLocation();
  }

  setDatos(BotDriverConfig configuracion) {
    if (configuracion.configuracionA != null) {
      print(configuracion.configuracionA?.listaZonaTrabajo.length);
      inputTarifaPorKmControllerA.text = (configuracion.configuracionA!.tarifa_por_km).toString();
      inputTarifaMinimaControllerA.text = (configuracion.configuracionA!.tarifa_minima).toString();
      inputTarifaMaximaControllerA.text = (configuracion.configuracionA!.tarifa_maxima).toString();
      inputDistanciaMaximaConductorOrigenControllerA.text = (configuracion.configuracionA!.distancia_maxima_conductor_origen).toString();
      inputDistanciaMaximaOrigenDestinoControllerA.text = (configuracion.configuracionA!.distancia_maxima_origen_destino).toString();
      inputMensajeWhatsappControllerA.text = (configuracion.configuracionA!.mensajeWhatsapp).toString();
      //inputFiltrarNotasControllerA.text = (configuracion?.configuracionA!.filtrarNotas).toString();
      inputFiltrarNotasControllerA.text = configuracion.configuracionA!.filtrarNotas.toString();

      String filtrarNotas = configuracion.configuracionA!.filtrarNotas;
      print(inputFiltrarNotasControllerA.text);
      print(filtrarNotas);
      selectedNotes = filtrarNotas.split(',').map((note) => note.trim()).toList();
      print(selectedNotes);


      configuracion.configuracionA?.listaZonaTrabajo.forEach((element) {
        LatLng circleCenter = LatLng(element.center.latitude, element.center.longitude);
        double circleRadius = element.radius;
        Map<String, dynamic> circleData = {
          'center': {'latitude': circleCenter.latitude, 'longitude': circleCenter.longitude},
          'radius': circleRadius
        };
        circleDataList.add(circleData);
        print('initialCircle');
        initialCircles.add(
          Circle(
            circleId: CircleId(element.center.latitude.toString() + element.center.longitude.toString()),
            center: LatLng(element.center.latitude, element.center.longitude),
            radius: element.radius,
            fillColor: Colors.blue.withOpacity(0.2),
            strokeWidth: 2,
            strokeColor: Colors.blue,
          ),
        );
      });
    }
  }

  void _navigateToMapScreen(BuildContext context) async {
    print('circleDataList0: ' + circleDataList.length.toString());

    final updatedCircles = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MapWithRadius(position: position, initialCircles: initialCircles),
      ),
    );
    if (updatedCircles != null && updatedCircles is Set<Circle>) {
      print(updatedCircles.toString());
      // Do something with the updated circles

      initialCircles.clear();
      initialCircles.addAll(updatedCircles);

      for (Circle circle in initialCircles) {
        LatLng circleCenter = circle.center;
        double circleRadius = circle.radius;
        Map<String, dynamic> circleData = {
          'center': {'latitude': circleCenter.latitude, 'longitude': circleCenter.longitude},
          'radius': circleRadius,
        };

        // Verifica si ya existe un círculo con los mismos datos en circleDataList
        bool circleExists = circleDataList.any((existingCircle) =>
        existingCircle['center']['latitude'] == circleData['center']['latitude'] &&
            existingCircle['center']['longitude'] == circleData['center']['longitude'] &&
            existingCircle['radius'] == circleData['radius']);

        if (!circleExists) {
          circleDataList.add(circleData);
        }
      }
    }
    print('circleDataList1: ' + circleDataList.length.toString());
  }

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context,listen: false);
    // Obtén los argumentos pasados a esta pantalla
    final Map<String, dynamic> arguments = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;

    // Accede a los valores pasados como parámetros
    Driver drive = arguments['drive'];


    return Scaffold(
      backgroundColor: secundary,
      appBar: AppBar(
        title: Text("Configuración del Bot"),
        backgroundColor: primary,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          color: Colors.white,
          child: CustomScrollView(
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate([
                  // Your widgets here
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                    child: Row(
                      children: const [
                        Text(
                          "Configuracion bot ",
                          style: TextStyle(
                            color: Colors.black87,
                            fontSize: 16.0,
                            fontFamily: "Brand Bold",
                          ),
                        ),
                        Icon(Icons.settings),
                      ],
                    ),
                  ),
                  const Divider(
                    height: 2.0,
                    thickness: 2.0,
                  ),

                  LabeledCheckbox(
                      keyboard: "number",
                      label: 'Tarifa por Km.',
                      label2: 'Bs.',
                      labelField: 'Ingrese tarifa',
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                      inputNumberController: inputTarifaPorKmControllerA),
                  LabeledCheckbox(
                      keyboard: "number",
                      label: 'Tarifa minima',
                      label2: 'Bs.',
                      labelField: 'Ingrese tarifa',
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                      inputNumberController: inputTarifaMinimaControllerA),
                  LabeledCheckbox(
                      keyboard: "number",
                      label: 'Tarifa maxima',
                      label2: 'Bs.',
                      labelField: 'Ingrese tarifa',
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                      inputNumberController: inputTarifaMaximaControllerA),
                  LabeledCheckbox(
                      keyboard: "number",
                      label: 'Distancia max. de conductor-origen',
                      label2: 'Km.',
                      labelField: 'Ingrese km.',
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                      inputNumberController: inputDistanciaMaximaConductorOrigenControllerA),
                  LabeledCheckbox(
                      keyboard: "number",
                      label: 'Distancia max. de origen-destino',
                      label2: 'Km.',
                      labelField: 'Ingrese km.',
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                      inputNumberController: inputDistanciaMaximaOrigenDestinoControllerA),
                  LabeledCheckbox(
                      keyboard: "text",
                      label: 'Mensaje para whatsapp',
                      label2: '',
                      labelField: 'Ingrese mensaje',
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      value: _isSelected,
                      onChanged: (bool newValue) {
                        setState(() {
                          _isSelected = newValue;
                        });
                      },
                      inputNumberController: inputMensajeWhatsappControllerA),

                  InkWell(
                    onTap: () {
                      //onChanged(!value);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(flex: 10, child: Text("Zona de trabajo")),
                          Expanded(
                            flex: 6,
                            child: TextButton(
                              onPressed: () async {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => VerMapaScreen(position: position, initialCircles: initialCircles),
                                    ));
                              },
                              child: Text("VER MAPAS"),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: TextButton(
                              onPressed: () => _navigateToMapScreen(context),
                              child: Text("SET MAPA"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  AddListNote(
                    keyboard: "text",
                    label: 'Filtrar notas',
                    label2: 'Bs.',
                    labelField: 'Ingrese tarifa',
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    value: _isSelected,
                    onNotesSelectedChanged: (notes) {
                      setState(() {
                        selectedNotes = notes;
                        print(notes);
                      });
                    },
                    notas: selectedNotes,
                    inputNumberController: inputFiltrarNotasControllerA,
                  ),
                  Divider(
                    height: 2.0,
                    thickness: 2.0,
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      Map<String, dynamic> locMap = {
                        "A": {
                          "tarifa_por_km": (inputTarifaPorKmControllerA.text == "" ? 0.0 : (double.parse(inputTarifaPorKmControllerA.text))),
                          "tarifa_minima": inputTarifaMinimaControllerA.text == "" ? 0.0 : (double.parse(inputTarifaMinimaControllerA.text)),
                          "tarifa_maxima": (inputTarifaMaximaControllerA.text == "" ? 0.0 : (double.parse(inputTarifaMaximaControllerA.text))),
                          "distancia_maxima_conductor_origen": (inputDistanciaMaximaConductorOrigenControllerA.text == "" ? 0.0 : (double.parse(inputDistanciaMaximaConductorOrigenControllerA.text))),
                          "distancia_maxima_origen_destino": (inputDistanciaMaximaOrigenDestinoControllerA.text == "" ? 0.0 : (double.parse(inputDistanciaMaximaOrigenDestinoControllerA.text))),
                          "mensaje_whatsapp": inputMensajeWhatsappControllerA.text,
                          "zona_trabajo": circleDataList,
                          "filtrar_notas": selectedNotes.join(", "), // Unir las notas con una coma y espacio
                          "status": false,
                        },
                        "flag_bot": false,
                      };

                      botDriverRef.doc(drive.id).set(locMap);

                      print(inputTarifaPorKmControllerA.text);
                      userProvider.fetchBotDriverData();
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("Bot configurado con exito")));
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red, // Cambia el color de fondo
                      padding: EdgeInsets.symmetric(vertical: 12),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Icon(Icons.save, color: Colors.white),
                        SizedBox(width: 8),
                        Text(
                          "Guardar",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontFamily: 'Brand Bold',
                          ),
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
