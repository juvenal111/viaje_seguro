import 'package:flutter/material.dart';
import 'package:zmovil_conductor/helpers/style.dart';

class EarningsScreen extends StatelessWidget {
  static const String idScreen = "earningsScreen";

  final double? earnings;

  EarningsScreen({this.earnings});

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> arguments = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;

    // Accede a los valores pasados como parámetros
    double earnings = arguments['earnings'];
    return Scaffold(
      appBar: AppBar(
        title: Text("Ganancias"),
        backgroundColor: primary,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "Total de Ganancias",
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20.0),
            Text(
              "\$$earnings", // Usar el valor de ganancias pasado como parámetro
              style: const TextStyle(
                fontSize: 36.0,
                color: Colors.green,
                fontWeight: FontWeight.bold,
              ),
            ),
            // SizedBox(height: 20.0),
            // ElevatedButton(
            //   onPressed: () {
            //     // Acción para ver el historial detallado de ganancias
            //     // Puedes navegar a otra pantalla o mostrar un diálogo aquí
            //   },
            //   child: Text("Ver Detalles"),
            // ),
          ],
        ),
      ),
    );
  }
}

