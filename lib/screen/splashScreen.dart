import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/assistant/authRepository.dart';
import 'package:zmovil_conductor/screen/carInfoScreen.dart';
import 'package:zmovil_conductor/screen/homePage.dart';
import 'package:zmovil_conductor/screen/loginPage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:after_layout/after_layout.dart'; // Importa el paquete after_layout

import '../provider/driver.dart';
import '../provider/user.dart';
import '../widget/pantallaCarga.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  static const String idScreen = "splash";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with AfterLayoutMixin<SplashScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool _locationPermissionGranted = false;
  bool _initialized = false;

  @override
  @override
  void initState() {
    super.initState();
    //authRepository = AuthRepository();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      init();
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // Llama al método init() aquí en lugar de dentro de didChangeDependencies()
  }

  Future<void> init() async {
    String? userId = _auth.currentUser?.uid;
    if (userId == null) {
      // Redirigir al usuario a la pantalla de inicio de sesión
      print('splashInit');
      //Navigator.pushNamedAndRemoveUntil(context, LoginPage.idScreen, (route) => false);
      // Navigator.pushNamedAndRemoveUntil(
      //   context, LoginPage.idScreen
      //   ),
    } else {
      Timer(const Duration(seconds: 5), () async {
        bool isCheckInfoCar = await Provider.of<DriverProvider>(context, listen: false).hasFilledCarInfo(userId);
        if (!isCheckInfoCar) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => CarInfoScreen(user: _auth.currentUser!, isfromSplash: true),
            ),
          );
        }
      });
      if (await _checkLocationPermission()) {
        // Si el permiso ha sido concedido, continua con la lógica actual
        setState(() {
          _initialized = true;
          _locationPermissionGranted = true; // Agregar esta línea para establecer _locationPermissionGranted como true
        });
      } else {
        // Si el permiso no ha sido concedido, muestra un diálogo de alerta al usuario
        showDialog(
          context: context,
          builder: (context) => _buildLocationPermissionDialog(),
        );
      }
    }
  }

  Future<bool> _checkLocationPermission() async {
    LocationPermission permission = await Geolocator.requestPermission();
    return permission == LocationPermission.always || permission == LocationPermission.whileInUse;
  }

  Widget _buildLocationPermissionDialog() {
    return AlertDialog(
      title: Text("Permiso de ubicación requerido"),
      content: Text("Por favor, concede el permiso de ubicación para continuar"),
      actions: [
        TextButton(
          child: Text("Cancelar"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        TextButton(
          child: Text("Abrir configuración"),
          onPressed: () {
            Geolocator.openAppSettings();
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    print('status splash:');
    print(userProvider.status);
    switch (userProvider.status) {
      case Status.Uninitialized:
        return PantallaCarga();
      case Status.Authenticated:
        return HomePage();
      // Pantalla de inicio si el usuario está autenticado
      case Status.Unauthenticated:
        return LoginPage(); // Pantalla de inicio de sesión si el usuario no está autenticado
      default:
        return PantallaCarga(); // Manejar otros estados si es necesario
    }

    // if (!_initialized) {
    //   return PantallaCarga();
    // } else {
    //   if (_locationPermissionGranted) {
    //     return StreamBuilder<User?>(
    //       stream: Provider.of<UserProvider>(context).userStream, // Use your UserProvider here
    //       builder: (context, snapshot) {
    //         if (snapshot.connectionState == ConnectionState.waiting) {
    //           return PantallaCarga();
    //         } else {
    //           // return HomePage();
    //           if (snapshot.hasData) {
    //             return HomePage();
    //           }else {
    //             return LoginPage();
    //           }
    //         }
    //       },
    //     );
    //   } else {
    //     // Si el permiso no ha sido concedido, muestra un diálogo de alerta al usuario
    //     return _buildLocationPermissionDialog();
    //   }
    // }
  }
}
