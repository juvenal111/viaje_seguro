import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/screen/registerPage.dart';
import 'package:zmovil_conductor/screen/splashScreen.dart';
import '../assistant/authRepository.dart';
import '../helpers/style.dart';
import '../provider/user.dart';
import '../widget/custom_text.dart';
import '../widget/loading.dart';

class LoginPage extends StatelessWidget {
  static const String idScreen = "loginPage";
  final _key = GlobalKey<ScaffoldState>();
  late AuthRepository authRepository = AuthRepository();
  bool isNoticeShown = false;
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<UserProvider>(context);
    if (!isNoticeShown) {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        showNotice(context);
        isNoticeShown = true; // Marcar que el aviso se ha mostrado
      });
    }
    return Scaffold(
      key: _key,
      backgroundColor: secundary,
      body: authProvider.status == Status.Authenticating
          ? Loading()
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 450,
                    color: white,
                    width: (MediaQuery.of(context).size.height / 100) * 100,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          "images/zdrive.png",
                          width: 230,
                          height: 230,
                        ),
                        const SizedBox(
                          height: 1.0,
                        ),
                        const Text(
                          "Login conductor",
                          style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: Container(
                      decoration: BoxDecoration(border: Border.all(color: white), borderRadius: BorderRadius.circular(5)),
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: TextFormField(
                          controller: authProvider.email,
                          style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                          decoration: const InputDecoration(
                              hintStyle: TextStyle(color: white),
                              border: InputBorder.none,
                              hintText: "Correo electronico",
                              icon: Icon(
                                Icons.email,
                                color: white,
                              )),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: Container(
                      decoration: BoxDecoration(border: Border.all(color: white), borderRadius: BorderRadius.circular(5)),
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: TextFormField(
                          controller: authProvider.password,
                          style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                          decoration: const InputDecoration(
                              hintStyle: TextStyle(color: white),
                              border: InputBorder.none,
                              hintText: "Password",
                              icon: Icon(
                                Icons.lock,
                                color: white,
                              )),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: GestureDetector(
                      onTap: () async {
                        await _requestLocationPermission();

                        if (!await authProvider.signIn()) {
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Login failed!")));
                          return;
                        }

                        authProvider.clearController();
                      },
                      child: Container(
                        decoration: BoxDecoration(color: primary, borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CustomText(
                                text: "Login",
                                color: secundary,
                                size: 22,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),

                  ///formulario registro aqui
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => RegisterPage()),
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomText(
                          text: "Registro aqui",
                          size: 20,
                          color: white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  void navigateToNextScreen(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => SplashScreen()),
    );
  }

  Future<void> _requestLocationPermission() async {
    final LocationPermission permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.whileInUse || permission == LocationPermission.always) {
      //_getUserLocation();
    } else {}
  }
  final ButtonStyle flatButtonStyle = TextButton.styleFrom(
    minimumSize: Size(88, 44),
    padding: EdgeInsets.symmetric(horizontal: 16.0),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(2.0)),
    ),
    backgroundColor: Colors.blue,
  );
  void showNotice(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Aviso destacado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Esta aplicación recopila datos de ubicación para habilitar la recepción de solicitudes de viaje y el cálculo de un radio de trabajo de posición dinámica, incluso cuando la aplicación está cerrada o no está en uso. Esto nos permite buscar carreras solo dentro de un radio específico.'),
                SizedBox(height: 10),
                Text('También compartimos esta información con nuestros socios pasajeros para proporcionar información más relevante sobre el tiempo y la distancia necesarios para atender su solicitud.'),
                SizedBox(height: 10),
                Text('Al hacer clic en "Entendido", aceptas que recopilemos y compartamos estos datos.'),

              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                print('Deny Button pressed');
                Navigator.of(context).pop();
                exit(0); // Cierra la aplicación
                // Aquí puedes manejar la lógica cuando el usuario niega la solicitud de permiso
              },
              child: Text('Denegar', style: TextStyle(color: secundary)),
            ),
            TextButton(
              onPressed: () {
                print('Accept Button pressed');
                Navigator.of(context).pop();
                // Aquí puedes manejar la lógica cuando el usuario acepta la solicitud de permiso
              },
              child: Text('Entendido', style: TextStyle(color: secundary)),
            ),
          ],
        );
      },
    );
  }

}
