import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/screen/splashScreen.dart';
import '../helpers/screen_navigation.dart';
import '../helpers/style.dart';
import '../provider/user.dart';
import '../widget/custom_text.dart';
import 'loginPage.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserProvider authProvider = Provider.of<UserProvider>(context);
    return Scaffold(
      backgroundColor: secundary,
      appBar: AppBar(
        title: Text('Registrarse'),
        backgroundColor: primary,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              color: white,
              height: 100,
            ),

            Container(
              color: white,
              width: (MediaQuery.of(context).size.height / 100) * 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset("images/zdrive.png", width: 100, height: 100,),
                  const SizedBox(
                    height: 1.0,
                  ),
                  const Text(
                    "Registro conductor",
                    style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Container(
              height: 40,
              color: white,
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.name,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Nombre completo",
                        hintText: "eg: Santos Enoque",
                        icon: Icon(Icons.person, color: white,)
                    ),
                  ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.email,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: const InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Correo electronico",
                        hintText: "santos@enoque.com",
                        icon: Icon(Icons.email, color: white,)
                    ),
                  ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.phone,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: const InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Numero de celular",
                        hintText: "+91 75698547",
                        icon: Icon(Icons.phone, color: white,)
                    ),
                  ),),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.password,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Contraseña",
                        hintText: "ingrese letras o numeros",
                        icon: Icon(Icons.lock, color: white,)
                    ),
                  ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: GestureDetector(
                onTap: ()async{
                  await _requestLocationPermission();

                  if(!await authProvider.signUp()){
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text("Registration failed!"))
                    );
                    return;
                  }
                  authProvider.clearController();
                  Navigator.pop(context);
                  //Navigator.pushNamedAndRemoveUntil(context, SplashScreen.idScreen, (route) => false);
                  // Navigator.of(context).pushAndRemoveUntil(
                  //   MaterialPageRoute(builder: (context) => SplashScreen()),
                  //       (Route<dynamic> route) => false,
                  // );
                  },
                child: Container(
                  decoration: BoxDecoration(
                      color: primary,
                      borderRadius: BorderRadius.circular(5)
                  ),
                  child: Padding(padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomText(text: "Registrar", color: secundary, size: 22,)
                      ],
                    ),),
                ),
              ),
            ),

            GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CustomText(text: "Ingrese aqui", size: 20,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<void> _requestLocationPermission() async {
    final LocationPermission permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.whileInUse || permission == LocationPermission.always) {
      //_getUserLocation();
    } else {}
  }
}