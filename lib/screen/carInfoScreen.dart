import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zmovil_conductor/helpers/style.dart';
import 'package:zmovil_conductor/screen/homePage.dart';
import 'package:zmovil_conductor/screen/splashScreen.dart';

import '../helpers/constants.dart';
import '../provider/user.dart';

class CarInfoScreen extends StatefulWidget {
  static const String idScreen = "carinfo";
  final User user;
  final bool? isfromSplash;
  CarInfoScreen({required this.user, this.isfromSplash});

  @override
  _CarInfoScreenState createState() => _CarInfoScreenState();
}

class _CarInfoScreenState extends State<CarInfoScreen> {
  final TextEditingController carModelTextEditingController = TextEditingController();
  final TextEditingController carNumberTextEditingController = TextEditingController();
  final TextEditingController carColorTextEditingController = TextEditingController();
  final List<String> _cities = ['elija', 'taxi', 'XL + 4 personas', 'moto', 'torito'];
  String _selectedCity = 'elija';

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text("Ingrese detalles de carro"),
        backgroundColor: primary,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Image.asset(
                "images/zdrive.png",
                width: 390.0,
                height: 250.0,
              ),
              const SizedBox(height: 12.0),
              TextField(
                controller: carModelTextEditingController,
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: secundary),
                  border: OutlineInputBorder(  // Agrega un borde
                    borderSide: BorderSide(color: Colors.grey),  // Establece el color del borde a gris
                  ),
                  labelStyle: TextStyle(color: secundary),
                  labelText: "Modelo de auto",
                  hintText: 'marca modelo año. (Ej. Toyota Hilux 2021)',
                ),
              ),
              const SizedBox(height: 10.0),
              TextField(
                controller: carNumberTextEditingController,
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: secundary),
                  border: OutlineInputBorder(  // Agrega un borde
                    borderSide: BorderSide(color: Colors.grey),  // Establece el color del borde a gris
                  ),
                  labelStyle: TextStyle(color: secundary),
                  labelText: "Placa de auto",
                ),
              ),
              SizedBox(height: 10.0),
              TextField(
                controller: carColorTextEditingController,
                decoration: InputDecoration(
                  hintStyle: TextStyle(color: secundary),
                  border: OutlineInputBorder(  // Agrega un borde
                    borderSide: BorderSide(color: Colors.grey),  // Establece el color del borde a gris
                  ),
                  labelStyle: TextStyle(color: secundary),
                  labelText: "Color de auto",
                ),
              ),
              SizedBox(height: 30.0),
              DropdownButton<String>(
                value: _selectedCity,
                onChanged: (String? newValue) {
                  setState(() {
                    _selectedCity = newValue!;
                  });
                },
                items: _cities.map((String city) {
                  return DropdownMenuItem<String>(
                    value: city,
                    child: Text(
                      city,
                      style: TextStyle(
                        color: Colors.blue, // Cambia esto por el color que desees para el texto
                        fontSize: 18, // Cambia esto por el tamaño de fuente que desees
                      ),
                    ),
                  );
                }).toList(),
                dropdownColor: Colors.grey, // Cambia esto por el color que desees para el fondo
              ),

              SizedBox(height: 42.0),
              TextButton(
                onPressed: () {
                  if (_isFormValid()) {
                    _saveDriverCarInfo(context);

                    Navigator.pop(context);
                  } else {
                    displayToastMessage("Por favor complete todos los campos", context);
                  }
                },
                style: TextButton.styleFrom(
                  backgroundColor: secundary,
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Siguiente",
                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Icon(
                      Icons.arrow_forward,
                      size: 26.0,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool _isFormValid() {
    return carModelTextEditingController.text.isNotEmpty &&
        carNumberTextEditingController.text.isNotEmpty &&
        carColorTextEditingController.text.isNotEmpty &&
        _selectedCity != 'elija';
  }

  void _saveDriverCarInfo(BuildContext context) {
    String userId = widget.user.uid;

    Map<String, dynamic> carInfoMap = {
      "car_color": carColorTextEditingController.text,
      "car_number": carNumberTextEditingController.text,
      "car_model": carModelTextEditingController.text,
      "type": _selectedCity=="XL + 4 personas" ? "xl_+_4_personas" : _selectedCity,
    };
    firebaseFiretore.collection('drivers').doc(userId).update({'info_car': carInfoMap});


  }

  displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }
}
