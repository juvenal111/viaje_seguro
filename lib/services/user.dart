//import '../Models/user.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zmovil_conductor/model/botDriverConfig.dart';

import '../helpers/constants.dart';
import '../model/botDriver.dart';
import '../model/driver.dart';
import '../model/userModel.dart';

class UserServices {
  String collection = "drivers";

  Future<void> createUser({
    required String id,
    required String name,
    required String email,
    required String phone,
    required int code,
    int votes = 0,
    int trips = 0,
    String rating = "0",
    Map? position,
    bool status=false,
    bool isBlocked=false,
  }) async {
    firebaseFiretore.collection(collection).doc(id).set({
      "name": name,
      "id": id,
      "phone": phone,
      "email": email,
      "votes": votes,
      "trips": trips,
      "rating": rating,
      "position": position,
      "status": status,
      "isBlocked": isBlocked,
      "code": code,
      "has_paid_bot": true
    });
  }

  void updateUserData(Map<String, dynamic> values) {
    firebaseFiretore.collection(collection).doc(values['id']).update(values);
  }

  Future<Driver> getUserById(String id) => firebaseFiretore.collection(collection).doc(id).get().then((doc) {
    return Driver.fromSnapshot(doc);
  });

  Future<BotDriverConfig?> getBotDriver(String userId) async {
    try {
      DocumentSnapshot botDriverSnapshot = await firebaseFiretore.collection(collection).doc(userId).get();

      if (botDriverSnapshot.exists) {
        // Utiliza el constructor factory para crear un objeto BotDriver a partir de los datos del snapshot
        return BotDriverConfig.fromMap(botDriverSnapshot.data() as DocumentSnapshot<Map<String, dynamic>>);
      } else {
        return null;
      }
    } catch (e) {
      print('Error obteniendo datos de botDriver: $e');
      return null;
    }
  }


  void addDeviceToken({required String token, required String userId}) {
    firebaseFiretore.collection(collection).doc(userId).update({"token": token});
  }
}
