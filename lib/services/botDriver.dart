import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zmovil_conductor/model/botDriverConfig.dart';

class BotDriverService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final String collection = "botDriver";




  Future<BotDriverConfig?> getBotDriverConfig(String userId) async {
    try {
      DocumentSnapshot<Map<String, dynamic>> snapshot =
      await _firestore.collection('botDriver').doc(userId).get();

      if (snapshot.exists) {
        // Si el documento existe, crea una instancia de BotDriverConfig desde el snapshot
        return BotDriverConfig.fromMap(snapshot);
      } else {
        // Si el documento no existe, devuelve null
        return null;
      }
    } catch (e) {
      print('Error obteniendo configuración de botDriver: $e');
      return null;
    }
  }


}
