import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

import '../helpers/constants.dart';

class RideRequestServices {
  final String collection = "rideRequests";
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>? _tarifaSugeridaSubscription;

  void startListeningToTarifaSugerida(String requestId, Function(double) onTarifaSugeridaChanged) {
    final DocumentReference<Map<String, dynamic>> rideRequestRef = _firestore.collection(collection).doc(requestId);
    _tarifaSugeridaSubscription = rideRequestRef.snapshots().listen((snapshot) {
      if (snapshot.exists) {
        double newTarifaSugerida = double.parse(snapshot.data()!['rideInfoMap']['tarifa_sugerida']);
        onTarifaSugeridaChanged(newTarifaSugerida);
      }
    });
  }

  void stopListeningToTarifaSugerida() {
    _tarifaSugeridaSubscription?.cancel();
  }
  Stream<QuerySnapshot> requestStream() {
    CollectionReference reference = FirebaseFirestore.instance.collection(collection);
    return reference.snapshots();
  }
}
