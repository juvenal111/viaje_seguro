

import 'package:url_launcher/url_launcher.dart';

class CallsAndMessagesService {
  void call(String number) {
    final Uri telLaunchUri = Uri(
      scheme: 'tel',
      path: number,
    );

    launch("tel:$telLaunchUri");
  }
  void sendSms(String number) => launch("sms:$number");
  void sendEmail(String email) => launch("mailto:$email");
}
