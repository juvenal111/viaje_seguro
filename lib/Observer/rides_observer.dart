class RidesObserver {
  final List<Function> _listeners = [];

  void addListener(Function listener) {
    _listeners.add(listener);
  }

  void notifyListeners() {
    for (var listener in _listeners) {
      listener();
    }
  }
  void cancel() {
    _listeners.clear();
  }
}
