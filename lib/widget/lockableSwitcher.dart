import 'package:flutter/cupertino.dart';

class LockableSwitcher extends StatelessWidget {
  final bool isLocked;
  final Widget child;

  LockableSwitcher({
    required this.isLocked,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: isLocked,
      child: child,
    );
  }
}
