import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:zmovil_conductor/helpers/style.dart';

import '../model/history.dart';

class HistoryItem extends StatelessWidget {
  final HistoryModel history;

  HistoryItem({required this.history});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Primera columna más ancha
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Column(
                      children: [
                        Image.asset('images/pickicon.png', height: 16, width: 16),
                      ],
                    ),
                    SizedBox(width: 5),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'ORIGEN',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: grey),
                        ),
                        GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text("Detalles del origen"),
                                  content: Text(
                                    history.pickupAddress,
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("Cerrar"),
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                          child: Text(
                            //history.pickupAddress.substring(0, 20) + '...',
                            //history.pickupAddress.length.toString(),
                              history.pickupAddress.length > 19 ? history.pickupAddress.substring(0, 20) + '...' : history.pickupAddress,
                            style: TextStyle(fontSize: 16.0),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        // Text(
                        //   history.pickupAddress,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                        // ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Column(
                      children: [
                        Image.asset('images/desticon.png', height: 16, width: 16),
                      ],
                    ),
                    SizedBox(width: 5),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'DESTINO',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: grey),
                        ),
                        GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text("Detalles del origen"),
                                  content: Text(
                                    history.dropOffAddress,
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("Cerrar"),
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                          child: Text(
                            //history.dropOffAddress,
                              //history.dropOffAddress.substring(0, 10) + '...',
                            history.dropOffAddress.length > 19 ? history.dropOffAddress.substring(0, 20) + '...' : history.dropOffAddress,
                            style: TextStyle(fontSize: 16.0),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        // Text(
                        //   history.dropOffAddress,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                        // ),
                      ],
                    ),
                  ],
                ),
                Text(
                  DateFormat('dd MMM yyyy, hh:mm a', 'es').format(DateTime.parse(history.createdAt)),
                  style: TextStyle(color: Colors.grey),
                ),
              ],
            ),
          ),
          SizedBox(width: 10), // Espacio entre las columnas
          // Segunda columna más estrecha
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center, // Centrar verticalmente
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                const Text(
                  'PRECIO',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: grey),
                ),
                Text(
                  '\$${history.precioAceptado.toStringAsFixed(2)}',
                  style: TextStyle(fontFamily: 'Brand Bold', fontSize: 16, color: Colors.black87),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
