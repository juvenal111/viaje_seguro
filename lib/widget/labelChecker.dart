import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LabeledCheckbox extends StatefulWidget {
  const LabeledCheckbox({
    required this.label2,
    required this.label,
    required this.labelField,
    required this.padding,
    required this.value,
    required this.onChanged,
    required this.inputNumberController,
    required this.keyboard,
  });

  final String keyboard;
  final String label2;
  final String label;
  final String labelField;
  final EdgeInsets padding;
  final bool value;
  final ValueChanged<bool> onChanged;
  final TextEditingController inputNumberController;

  @override
  _LabeledCheckboxState createState() => _LabeledCheckboxState();
}

class _LabeledCheckboxState extends State<LabeledCheckbox> {
  TextEditingController _modalTextController = TextEditingController();

  void _showModall() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Ingrese dato'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: _modalTextController,
                keyboardType: widget.keyboard == "number"
                    ? TextInputType.numberWithOptions(decimal: true) // Permitir números con decimales
                    : TextInputType.text,
                inputFormatters: widget.keyboard == "number"
                    ? [FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*'))] // Permitir números con decimales
                    : null,
                decoration: InputDecoration(labelText: 'Modal Content'),
              ),              ElevatedButton(
                onPressed: () {
                  setState(() {
                    widget.inputNumberController.text = _modalTextController.text; // Update the TextField's value
                    _modalTextController.clear(); // Clear the modal TextField's value
                  });
                  Navigator.of(context).pop(); // Close the modal
                },
                child: Text('Cerrar'),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onChanged(!widget.value);
      },
      child: Padding(
        padding: widget.padding,
        child: Row(
          children: <Widget>[
            Expanded(flex: 10, child: Text(widget.label)),
            Expanded(
              flex: 3,
              child: Text(
                widget.inputNumberController.text == '0.0' ? '' : widget.inputNumberController.text, // Mostrar el texto del controlador
                style: const TextStyle(
                  fontSize: 14.0,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(Icons.add),
                onPressed: _showModall,
              ),
            ),
          ],
        ),
      ),
    );
  }


  @override
  void dispose() {
    _modalTextController.dispose();
    super.dispose();
  }
}





