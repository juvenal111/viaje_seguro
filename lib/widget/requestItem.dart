import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:zmovil_conductor/screen/detalleCarreraPage.dart';
import '../helpers/style.dart';
import '../model/botDriverConfig.dart';
import '../model/driver.dart';
import '../model/rideRequests.dart';

class RequestItem extends StatelessWidget {
  final RideRequest listaPasajero;
  final String hash;
  final Driver driver;
  final VoidCallback stopRidesSubscription;
  final BotDriverConfig? driverConfig;
  final Position currrentPosition;
  final VoidCallback resetRidesCallback; // Agrega esta línea
  RequestItem(
      {required this.listaPasajero,
      required this.hash,
      required this.driver,
      required this.stopRidesSubscription,
      required this.resetRidesCallback,
      required this.currrentPosition,
      this.driverConfig});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        //tarifa_cambiada_rider = int.parse(listaPasajero.rideDetails.tarifaOfertada);
        stopRidesSubscription(); // Llamar al callback para detener _ridesSubscription

        var res = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetalleCarreraScreen(
                      currrentPosition: currrentPosition,
                      rideDetails: listaPasajero,
                      hash: hash,
                      driver: driver,
                      config: driverConfig,
                    )));
        print(res);
        if (res == "finished" || res == "oferta_rechazada" || res == "cancelar") {
          resetRidesCallback(); // Llama al callback para resetear _rides
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black38,
                blurRadius: 16.0,
                spreadRadius: 0.5,
                offset: Offset(0.7, 0.7),
              ),
            ],
          ),
          height: (MediaQuery.of(context).size.height / 100) * 25,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Column(
              children: [
                Expanded(
                  flex: 5,
                  child: Container(
                    child: Row(
                      children: [
                        //column profile
                        Expanded(
                          flex: 2,
                          child: Container(
                            //color: Colors.,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(13)),
                                  child: Container(
                                    height: 55,
                                    width: 55,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: secundary,
                                    ),
                                    child: Image.asset(
                                      "images/pasajero.png",
                                      //height: 50,
                                      //width: 50,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      listaPasajero.riderName,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        fontSize: 10.0,
                                        color: Colors.black38,
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        //color: LightColor.terciary,
                                        child: Text(
                                          'Carreras.: ${listaPasajero.carrerasTomadas}',
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                            fontSize: 10.0,
                                            color: Colors.black38,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                              ],
                            ),
                          ),
                        ),
                        //columns tarifa y otros datos
                        Expanded(
                          flex: 8,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15.0, 0.0, 00.0, 0.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 7,
                                        child: Container(
                                          //color: Colors.green,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              const Text(
                                                "`Pasajero - Destino",
                                                style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                              ),
                                              Expanded(
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Expanded(
                                                      child: Row(
                                                        children: [
                                                          Image.asset(
                                                            "images/distancia.png",
                                                            height: 15.0,
                                                            //width: 16.0,
                                                          ),
                                                          SizedBox(
                                                            width: 1,
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                              '${listaPasajero.distanceText}',
                                                              style: Theme.of(context).textTheme.subtitle1,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(width: 2),
                                                    Expanded(
                                                      child: Row(
                                                        children: [
                                                          Image.asset(
                                                            "images/tiempo.png",
                                                            height: 30.0,
                                                            //width: 16.0,
                                                          ),
                                                          SizedBox(
                                                            width: 1,
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                              '${listaPasajero.durationText}',
                                                              style: Theme.of(context).textTheme.subtitle1,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Container(
                                          //color: Colors.red,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                                                //color: Colors.yellow,
                                                child: Image.asset(
                                                  "images/pagar.png",
                                                  height: 25.0,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 1,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                  child: Text(
                                                    '${double.parse(listaPasajero.tarifaSugerida)!.toStringAsFixed(2)} Bs.',
                                                    style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold, color: Colors.red),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 5.0,
                                ),

                                ///pasajero
                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            const Text(
                                              "Pasajero",
                                              style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                            ),
                                            Expanded(
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    "images/pickicon.png",
                                                    height: 15.0,
                                                    //width: 16.0,
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  Expanded(
                                                    child: Text(
                                                      listaPasajero.pickupAddress!,
                                                      style: Theme.of(context).textTheme.subtitle1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 5.0,
                                ),

                                ///DESTINO

                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Destino",
                                              style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                            ),
                                            Expanded(
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    "images/desticon.png",
                                                    height: 15.0,
                                                    //width: 16.0,
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Expanded(
                                                      child: Text(
                                                    listaPasajero.dropoffAddress!,
                                                    style: Theme.of(context).textTheme.subtitle1,
                                                  )),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // Expanded(
                                //   child: Row(
                                //     children: [
                                //       Expanded(
                                //         flex: 3,
                                //         child: Column(
                                //           mainAxisSize: MainAxisSize.min,
                                //           crossAxisAlignment: CrossAxisAlignment.start,
                                //           children: <Widget>[
                                //             Text(
                                //               "Categoria:",
                                //               style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                //             ),
                                //             Expanded(
                                //               child: Row(
                                //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //                 children: [
                                //                   Expanded(
                                //                     child: Row(
                                //                       children: [
                                //                         Expanded(
                                //                           child: Text(
                                //                             listaPasajero.tipoVehiculo,
                                //                             style: Theme.of(context).textTheme.titleMedium,
                                //                           ),
                                //                         ),
                                //                       ],
                                //                     ),
                                //                   ),
                                //                   //SizedBox(width: 2),
                                //                 ],
                                //               ),
                                //             ),
                                //           ],
                                //         ),
                                //       ),
                                //     ],
                                //   ),
                                // ),
                                const SizedBox(
                                  height: 5.0,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                      child: Column(
                        //color: Colors.black,
                        children: [
                          // Expanded(
                          //   child: Row(
                          //     children: [
                          //       Expanded(
                          //         flex: 3,
                          //         child: Column(
                          //           mainAxisSize: MainAxisSize.min,
                          //           crossAxisAlignment: CrossAxisAlignment.start,
                          //           children: <Widget>[
                          //             Text(
                          //               "Categoria:",
                          //               style: TextStyle(fontSize: 10.0, color: Colors.black38),
                          //             ),
                          //             Expanded(
                          //               child: Row(
                          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   Expanded(
                          //                     child: Row(
                          //                       children: [
                          //                         Expanded(
                          //                           child: Text(
                          //                             listaPasajero.tipoVehiculo,
                          //                             style: Theme.of(context).textTheme.titleMedium,
                          //                           ),
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                   //SizedBox(width: 2),
                          //                 ],
                          //               ),
                          //             ),
                          //           ],
                          //         ),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                // Text(
                                //   "•",
                                //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.0),
                                // ),

                                Expanded(
                                  flex: 6,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Conductor - Pasajero",
                                        style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                      ),
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    "images/distancia.png",
                                                    height: 15.0,
                                                    //width: 16.0,
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        for (var conductor in listaPasajero.conductoresDistanciaTiempo.values)
                                                          if (conductor['conductor_code'] == driver.code)
                                                            Text(
                                                              '${conductor['distancia_conductor_pasajero']}', // Muestra la distancia real
                                                              style: Theme.of(context).textTheme.titleMedium,
                                                            ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            //SizedBox(width: 2),
                                            Expanded(
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    "images/tiempo.png",
                                                    height: 30.0,
                                                    //width: 16.0,
                                                  ),
                                                  SizedBox(
                                                    width: 1,
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        for (var conductor in listaPasajero.conductoresDistanciaTiempo.values)
                                                          if (conductor['conductor_code'] == driver.code)
                                                            Text(
                                                              '${conductor['tiempo_conductor_pasajero']}', // Muestra la distancia real
                                                              style: Theme.of(context).textTheme.titleMedium,
                                                            ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Tipo carrera:",
                                        style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                      ),
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      listaPasajero.tipoCarrera,
                                                      style: Theme.of(context).textTheme.subtitle1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //SizedBox(width: 2),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,

                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "Deseo",
                                        style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                      ),
                                      Image.asset(
                                        "images/icono_deseo.png",
                                        height: 20.0,
                                      ),
                                    ],
                                  ),
                                ),
                                //SizedBox(width: 5,),
                                Expanded(
                                  flex: 9,
                                  child: Text(
                                    '${listaPasajero.deseo}',
                                    style: Theme.of(context).textTheme.titleMedium,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
