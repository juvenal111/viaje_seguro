import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddListNote extends StatefulWidget {
  final String keyboard;
  final String label2;
  final String label;
  final String labelField;
  final EdgeInsets padding;
  final bool value;
  final ValueSetter<List<String>> onNotesSelectedChanged; // Agrega esta línea
  final TextEditingController inputNumberController;
  final List<String> notas;


  const AddListNote({
    required this.label2,
    required this.label,
    required this.labelField,
    required this.padding,
    required this.value,
    required this.onNotesSelectedChanged, // Agrega esta línea
    required this.inputNumberController,
    required this.keyboard,
    required this.notas,
  });



  @override
  _LabeledCheckboxState createState() => _LabeledCheckboxState();
}

class _LabeledCheckboxState extends State<AddListNote> {
  TextEditingController _modalTextController = TextEditingController();
  List<String> selectedNotes = []; // Lista para almacenar las notas seleccionadas

  void _showModall() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Ingrese dato'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: _modalTextController,
                keyboardType: widget.keyboard == "number"
                    ? TextInputType.numberWithOptions(decimal: true) // Permitir números con decimales
                    : TextInputType.text,
                inputFormatters: widget.keyboard == "number"
                    ? [FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*'))] // Permitir números con decimales
                    : null,
                decoration: InputDecoration(labelText: 'Modal Content'),
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    selectedNotes.clear();
//                    List<String> notas = notasString.split(',').where((note) => note.trim().isNotEmpty).toList();

                    selectedNotes = (_modalTextController.text).split(',').where((note) => note.trim().isNotEmpty).toList();
                    _modalTextController.clear();
                    widget.onNotesSelectedChanged(selectedNotes); // Llama a la función aquí
                  });
                  Navigator.of(context).pop();
                },
                child: Text('Cerrar'),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    selectedNotes = widget.notas;
    _modalTextController.text = widget.notas.join(', ');

    return InkWell(
      onTap: () {
        widget.onNotesSelectedChanged(selectedNotes);
      },
      child: Padding(
        padding: widget.padding,
        child: Row(
          children: <Widget>[
            Expanded(flex: 10, child: Text(widget.label)),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: selectedNotes.map((note) {
                  return Text(
                    note,
                    style: const TextStyle(
                      fontSize: 14.0,
                    ),
                  );
                }).toList(),
              ),
            ),
            Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(Icons.add),
                onPressed: _showModall,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _modalTextController.dispose();
    super.dispose();
  }
}
