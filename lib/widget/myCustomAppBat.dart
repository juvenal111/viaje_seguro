import 'package:flutter/material.dart';
import 'package:zmovil_conductor/helpers/style.dart';

class MyCustomAppBar extends StatelessWidget implements PreferredSize {
  final AppBar appBar;
  final Widget bottomWidget;
  final double height;

  const MyCustomAppBar({

    required this.appBar,
    required this.bottomWidget,
    required this.height,
  }) ;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Stack(
        children: [
          Container(
            height: height / 1.2,
            child: appBar,
          ),
          Positioned(
            left: 60,
            right: 30,
            bottom: 0,
            top: 10,
            child: Material(
              elevation: 0,
              color: Colors.transparent, // Establecer el color de fondo en transparente
              child: bottomWidget,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget get child => this;

  @override
  Size get preferredSize => Size.fromHeight(height);
}