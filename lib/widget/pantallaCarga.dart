import 'package:flutter/material.dart';

import '../helpers/style.dart';

class PantallaCarga extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: secundary, // Cambia el color de fondo a blanco utilizando RGB
      child: Stack(
        children: [
          Center(child: Image.asset('images/zdrive.png')),
          Center(child: CircularProgressIndicator()),
        ],
      ),
    );
  }
}
