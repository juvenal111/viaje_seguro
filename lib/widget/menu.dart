import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zmovil_conductor/provider/user.dart';
import 'package:zmovil_conductor/screen/botPage.dart';
import 'package:zmovil_conductor/screen/earningScreen.dart';
import 'package:zmovil_conductor/widget/simpleDialogItem.dart';


import '../model/driver.dart';
import '../provider/appData.dart';
import '../screen/historyScreen.dart';
import '../screen/profileScreen.dart';
import '../screen/ratingView.dart';
import 'Divider.dart';

class MenuWidget extends StatelessWidget {
  final VoidCallback onSignOutCallback; // Accept a VoidCallback
  final Driver modelUser;
  MenuWidget({required this.modelUser, required this.onSignOutCallback});
  @override
  Widget build(BuildContext context) {
    final SimpleDialog dialogReportarProblemas = SimpleDialog(
      title: Text('Numeros de soporte'),
      children: [
        SimpleDialogItem(
          color: Colors.orange,
          text: '+59162023317',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber(context, '+59163570398');
          },
        ),
        SimpleDialogItem(
          color: Colors.green,
          text: '+59162023317',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber(context, '+59162023317');
          },
        ),
        // SimpleDialogItem(
        //   color: Colors.blue,
        //   text: '+59176038385',
        //   onPressed: () {
        //     Navigator.pop(context);
        //     openwhatsappNumber(context, '+59176038385');
        //   },
        // ),
      ],
    );
    return
      Container(
        color: Colors.white,
        width: 255.0,
        child: Drawer(
          child: ListView(
            children: [
              Container(
                height: 165.0,
                child: DrawerHeader(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset("images/user_icon.png", height: 65.0, width: 65.0),
                      const SizedBox(
                        width: 16.0,
                      ),
                      Text(
                        modelUser?.name ?? '',
                        style: TextStyle(fontSize: 16.0, fontFamily: "Brand Bold"),
                      ),

                      SizedBox(height: 6.0),
                      Text("Carreras tomadas: " + Provider.of<AppData>(context, listen: false).countTrips.toString()),

                      // SizedBox(height: 6.0),
                      // Text("Visitar perfil"),
                    ],
                  ),
                ),
              ),
              const DividerWidget(),
              const SizedBox(
                height: 12.0,
              ),
              /**VIAJES**/
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                      context, HistoryScreen.idScreen);
                },
                child: const ListTile(
                  leading: Icon(Icons.history),
                  title: Text(
                    "Viajes",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
              ///GANANCIAS
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                    context, EarningsScreen.idScreen,                     arguments: {
                    'earnings': modelUser.earnings, // Pasa los parámetros aquí
                  },
                  );
                },
                child: const ListTile(
                  leading: Icon(Icons.attach_money),
                  title: Text(
                    "Ganancias",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
              /**REPORTAR**/
              GestureDetector(
                onTap: () {
                  var retorno = showDialog<void>(context: context, builder: (context) => dialogReportarProblemas);
                },
                child: const ListTile(
                  leading: Icon(Icons.report_problem), // Icono para "Reportar"
                  title: Text(
                    "Reportar",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
              /**CALIFICACION**/
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, RatingView.idScreen);
                },
                child: const ListTile(
                  leading: Icon(Icons.star), // Icono para "Calificación"
                  title: Text(
                    "Calificación",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
              /**CUENTA****/
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, ProfileScreen.idScreen);
                },
                child: const ListTile(
                  leading: Icon(Icons.account_circle), // Icono para "Cuenta"
                  title: Text(
                    "Cuenta",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
              /**CONFIGURACION BOT****/
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    BotPage.idScreen,
                    arguments: {
                      'drive': modelUser, // Pasa los parámetros aquí
                    },
                  );
                },
                child: const ListTile(
                  leading: Icon(Icons.settings), // Icono para "Cuenta"
                  title: Text(
                    "Configuracion bot",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
              /**CERRAR SESION****/
              GestureDetector(
                onTap: () async {
                  Navigator.pop(context); // Cerrar el menú
                  onSignOutCallback(); // Ejecutar el callback al cerrar sesión
                  //
                  // await userProvider.signOut();
                  // await Navigator.pushNamedAndRemoveUntil(
                  //   context,
                  //   LoginPage.idScreen,
                  //       (route) => false,
                  // );
                },
                child: const ListTile(
                  leading: Icon(Icons.logout),
                  title: Text(
                    "Cerrar sesion",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
  }

  void openwhatsappNumber(BuildContext context, String numero) async {
    //print(driverPhone);
    String whatsapp = "+591" + "driverPhone";
    var whatsappURl_android = "whatsapp://send?phone=" + numero + "&text=Hola";
    var whatappURL_ios = "https://wa.me/$whatsapp?text=${Uri.parse("hello")}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunch(whatappURL_ios)) {
        await launch(whatappURL_ios, forceSafariVC: false);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("whatsapp no instalado")));
      }
    } else {
      // android , web
      if (await canLaunch(whatsappURl_android)) {
        await launch(whatsappURl_android);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("whatsapp no instalado")));
      }
    }
  }
}